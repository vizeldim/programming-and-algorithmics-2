#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

class CBigInt
{
  public:
    // default constructor
    CBigInt();
    // int constructor
    CBigInt(int num);
    // string constructor
    CBigInt(const char * num);
     CBigInt(string num);

    // operator +, any combination {CBigInt/int/string} + {CBigInt/int/string}
    friend CBigInt operator +(const CBigInt & left, const CBigInt & right);

    // operator *, any combination {CBigInt/int/string} * {CBigInt/int/string}
    friend CBigInt operator *(const CBigInt & left, const CBigInt & right);

    // operator +=, any of {CBigInt/int/string}
    CBigInt& operator += (const CBigInt & other);

    // operator *=, any of {CBigInt/int/string}
    CBigInt& operator *= (const CBigInt & other);

    // comparison operators, any combination {CBigInt/int/string} {<,<=,>,>=,==,!=} {CBigInt/int/string}
    friend bool operator ==(const CBigInt & left, const CBigInt & right);
    friend bool operator !=(const CBigInt & left, const CBigInt & right);
    friend bool operator >(const CBigInt & left, const CBigInt & right);
    friend bool operator <(const CBigInt & left, const CBigInt & right);
    friend bool operator >=(const CBigInt & left, const CBigInt & right);
    friend bool operator <=(const CBigInt & left, const CBigInt & right);
  
    // output operator <<
    friend ostream& operator<<(ostream& os, const CBigInt& obj);
    // input operator >>
    friend istream& operator>>(istream& is, CBigInt& obj);

    bool isPositive() const{return positive;}
  private:
    // todo
    bool isNumString(string snum);
    string m_num;
    bool positive;
};


string mToString(int num){
  string snum;
  if(num<0){num*=-1;}
  while(num){
    snum.insert(snum.begin(),(num % 10) + '0');
    num/=10;
  }
  return snum;
}


bool CBigInt::isNumString(string snum){
  for(unsigned int i = 0; i < snum.length(); i++){
    if(!(isdigit(snum[i]))){
      return false;
    }
  }
  return true;
}

void formatToDigit(string & snum){
  unsigned int i = 0;
  while(snum[i] == '0' && i<snum.length()-1){
    i++;
  }
  snum = snum.substr(i);
}

ostream& operator <<(ostream& os, const CBigInt& obj){
  if(!(obj.isPositive())){os << '-';}
  os << obj.m_num; 
  return os;
}

istream& operator >>(istream& is,  CBigInt& obj){
  string tmp;
  is >> tmp;
  unsigned int i;
  for(i = 0; i < tmp.length(); i++){
    if(!(isdigit(tmp[i]))){
      break;
    }
  }
  if(i==0 && !(isdigit(tmp[i]))){
    is.setstate(ios_base::failbit);
  }else{
    obj.m_num = tmp.substr(0, i);
  }
  return is;
}


/*----------------------CONSTRUCTOR------------------------------*/
CBigInt::CBigInt() : positive(true){
  m_num.push_back('0');
}

CBigInt::CBigInt(int num){
  positive = (num >= 0);
  m_num = mToString(num);
}

CBigInt::CBigInt(const char * num){
  string snum(num); 
  positive = true;
  if(snum[0]=='-')
  {
    snum = snum.substr(1);
    positive = false;
  }
  if(!isNumString(snum)){
    cout << snum <<endl;
    throw invalid_argument("not a number");
  }
  formatToDigit(snum);
  if(snum == "0"){
    positive = true;
  }
  m_num = snum;
}
CBigInt::CBigInt(string snum){ 
  positive = true;
  if(snum[0]=='-')
  {
    snum = snum.substr(1);
    positive = false;
  }
  if(!isNumString(snum)){
    cout << snum <<endl;
    throw invalid_argument("not a number");
  }
  formatToDigit(snum);
  if(snum == "0"){
    positive = true;
  }
  m_num = snum;
}
/*-------------------------------------SUM------------------------------------*/

CBigInt operator +(const CBigInt & left, const CBigInt & right){
  string nNum;
  bool p = true;
  string bNum = left.m_num;
  string sNum = right.m_num;
  if( (sNum.length() > bNum.length()) || (sNum.length()== bNum.length() && sNum > bNum) ){
    bNum = right.m_num;
    sNum = left.m_num;
    if(!right.positive){
      p = false;
    }
  }else if(!left.positive){
    p = false;
  }
  int sNumLen = sNum.length();
  int bNumLen = bNum.length();
  int zb = 0;

  if( (left.positive && right.positive) || (!left.positive && !right.positive) )
  {
    int i = sNumLen - 1;
    int j = bNumLen - 1;
    while(i>=0){
      nNum.insert(nNum.begin(), bNum[j] + sNum[i] - '0' + zb);
      zb = 0;
      if(nNum[0] > '9')
      {
        nNum[0] -= 10;
        zb = 1;
      }
      i--;
      j--;
    }

    while(j>=0)
    {
      nNum.insert(nNum.begin(), bNum[j] + zb);
      zb = 0;
      if(nNum[0] > '9')
      {
        nNum[0] -= 10;
        zb = 1;
      }
      j--;
    }

    if(zb == 1)
    {
      nNum.insert(nNum.begin(),'1');
    }
    if(!left.positive){p=false;}
  }
  else{
    int i = sNumLen - 1;
    int j = bNumLen - 1;
    while(i>=0){
      
      nNum.insert(nNum.begin(), '0' + bNum[j] - sNum[i] + zb);
      zb = 0;
      if(nNum[0] < '0')
      {
        if(i!=0 || j > i){
          nNum[0] += 10;
          zb = -1;
        }
        else{
          nNum[0] *= -1;
        }
      }
      i--;
      j--;
    }

    while(j>=0)
    {
      nNum.insert(nNum.begin(), bNum[j] + zb);
      zb = 0;
      if(nNum[0] < '0')
      {
        nNum[0] += 10;
        zb = 1;
      }
      j--;
    }
  }
 
  
  CBigInt nObj(nNum);
   if(nObj.m_num=="0"){p=true;}
  nObj.positive = p;
  return nObj;
}



/*------------------------------MULTIPLE-----------------------------*/


CBigInt operator *(const CBigInt & left, const CBigInt & right){
 CBigInt nObj;
  string snum = right.m_num;
  string n_num;

  int zb = 0;
  int snumLen = snum.length() - 1;
  int m_numLen =  left.m_num.length() - 1;
  
  string maxNum = (snumLen >= m_numLen)? snum : left.m_num;
  string minNum = (snumLen < m_numLen)? snum : left.m_num;

  int maxNumLen = maxNum.length() - 1;
  int minNumLen = minNum.length() - 1;

  for(int i = minNumLen; i>=0; i-- ){
    
    zb =0;
    n_num.clear();
    for(int j = maxNumLen; j>=0; j--){
      
      int res = (minNum[i] - '0') * (maxNum[j] - '0');
      n_num.insert(n_num.begin(), '0'+(res%10) + zb);
      zb=0;
      if(n_num[0] > '9')
      {
        n_num[0] -= 10;
        zb = 1;
      }
      if(res > 9){
        zb += res / 10;
      }
    }
    if(zb!=0){
       n_num.insert(n_num.begin(), '0'+zb);
    }
    for(int q = 0; q<minNumLen-i;q++){
      n_num.push_back('0');
    }
    nObj = nObj + n_num;
  }
  if( (!(right.positive) && left.positive) || (right.positive && !left.positive)){
    nObj.positive = false;
  }
  if(nObj.m_num=="0"){nObj.positive=true;}
  return nObj;
}

CBigInt & CBigInt::operator += (const CBigInt & other){
  *this = *this + other;
  return *this;
}

CBigInt & CBigInt::operator *= (const CBigInt & other){
  *this = (*this) * other;
  return *this;
}

/*-------------------------CMP_OPERATORS----------------------*/
bool operator ==(const CBigInt & left, const CBigInt & right){
  return ((left.positive && right.positive) || (!left.positive && !right.positive)) && left.m_num == right.m_num;
}
bool operator !=(const CBigInt & left, const CBigInt & right){
  return !(left == right);
}
bool operator >(const CBigInt & left, const CBigInt & right){
    return ( left.positive && right.positive && ( left.m_num.length() > right.m_num.length() || (left.m_num.length() == right.m_num.length() && left.m_num > right.m_num) ) )||
             (left.positive && !right.positive)||
             (!left.positive && !right.positive && ( left.m_num.length() < right.m_num.length() || ( left.m_num.length() == right.m_num.length() && left.m_num < right.m_num)) );
}
bool operator <(const CBigInt & left, const CBigInt & right){
  return (!(left == right) && !(left > right));
}
bool operator >=(const CBigInt & left, const CBigInt & right){
  return ((left == right) || (left > right));
}
bool operator <=(const CBigInt & left, const CBigInt & right){
  return ((left == right) || (left < right));
}


#ifndef __PROGTEST__
static bool equal ( const CBigInt & x, const char * val )
{ 
  ostringstream oss;
  oss << x;
  return oss . str () == val;
}

int main ( void )
{

 CBigInt a, b;
  istringstream is;
a=10;
  a += 20;

  assert ( equal ( a, "30" ) );
 a *= 5;
  
  
  assert ( equal ( a, "150" ) );
  
  b = a + 3;

  assert ( equal ( b, "153" ) );

 b = a * 7;
  assert ( equal ( b, "1050" ) );
  assert ( equal ( a, "150" ) );

 a = 10;
 
  a += -20;
   
  assert ( equal ( a, "-10" ) );
 
  a *= 5;
  assert ( equal ( a, "-50" ) );
  b = a + 73;
  assert ( equal ( b, "23" ) );
  b = a * -7;
  assert ( equal ( b, "350" ) );
  assert ( equal ( a, "-50" ) );

  a = 120456840;
  a *= -707;

  a = "12345678901234567890";
 
  a += "-99999999999999999999";
  assert ( equal ( a, "-87654321098765432109" ) );
   a *= "54321987654321987654";
   
  assert ( equal ( a, "-4761556948575111126880627366067073182286" ) );
 a *= 0;
  assert ( equal ( a, "0" ) );
  a = 10;
  b = a + "400";
  assert ( equal ( b, "410" ) );
  b = a * "15";
  assert ( equal ( b, "150" ) );
  assert ( equal ( a, "10" ) );

  is . clear ();
  is . str ( " 1234" );
  assert ( is >> b );

 assert ( equal ( b, "1234" ) );
  is . clear ();
  is . str ( " 12 34" );

 assert ( is >> b );
  assert ( equal ( b, "12" ) );
  is . clear ();
  is . str ( "999z" );
  
  assert ( is >> b );
    
  assert ( equal ( b, "999" ) );
  is . clear ();
  is . str ( "abcd" );

  assert ( ! ( is >> b ) );
  is . clear ();
  is . str ( "- 758" );
  assert ( ! ( is >> b ) );
 try
  {
    a = "-xyz";
    assert ( "missing an exception" == NULL );
  }
  catch ( const invalid_argument & e )
  {
  }

  a = "73786976294838206464";
  assert ( a < "1361129467683753853853498429727072845824" );
  assert ( a <= "1361129467683753853853498429727072845824" );
  assert ( ! ( a > "1361129467683753853853498429727072845824" ) );
  assert ( ! ( a >= "1361129467683753853853498429727072845824" ) );
  assert ( ! ( a == "1361129467683753853853498429727072845824" ) );
  assert ( a != "1361129467683753853853498429727072845824" );
  assert ( ! ( a < "73786976294838206464" ) );
  assert ( a <= "73786976294838206464" );
  assert ( ! ( a > "73786976294838206464" ) );
  assert ( a >= "73786976294838206464" );
  assert ( a == "73786976294838206464" );
  assert ( ! ( a != "73786976294838206464" ) );
  assert ( a < "73786976294838206465" );
  assert ( a <= "73786976294838206465" );
  assert ( ! ( a > "73786976294838206465" ) );
  assert ( ! ( a >= "73786976294838206465" ) );
  assert ( ! ( a == "73786976294838206465" ) );
  assert ( a != "73786976294838206465" );
  a = "2147483648";
  assert ( ! ( a < -2147483648 ) );
  assert ( ! ( a <= -2147483648 ) );
  assert ( a > -2147483648 );
  assert ( a >= -2147483648 );
  assert ( ! ( a == -2147483648 ) );
  assert ( a != -2147483648 );

  return 0;
}
#endif /* __PROGTEST__ */
