#ifndef __PROGTEST__
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */


//------------------CEmpty--------------------------------------------------
    class CEmpty
    {
        public:
            virtual void PrintLine( ostream & os, int lineNum, int w, int h );
            virtual unsigned int GetHeight();
            virtual unsigned int GetWidth();
            virtual CEmpty * MakeCopy() const;
            virtual ~CEmpty(){}
            CEmpty(){}
            CEmpty(const CEmpty & other){}
            CEmpty & operator=(const CEmpty & other){return *this;}
    };

    void CEmpty::PrintLine( ostream & os, int lineNum, int w, int h )
    {
        for(int i = 0; i < w; ++i)
        {
            os << " ";
        }
    }

    unsigned int CEmpty::GetHeight()
    {
        return 0;
    }

    unsigned int CEmpty::GetWidth()
    {
        return 0;
    }

    CEmpty * CEmpty::MakeCopy() const
    {
        return new CEmpty();
    }
//------------------CContentCell--------------------------------------------
    class CContentCell : public CEmpty
    {
        public:
            vector<string> m_Content;
            unsigned int GetHeight() override;
    };
    
    unsigned int CContentCell::GetHeight()
    {
        return m_Content.size();
    }
//------------------CImage--------------------------------------------------
    class CImage : public CContentCell
    {
        public:
            void PrintLine( ostream & os, int lineNum, int w, int h ) override;
            unsigned int GetWidth() override;
            CImage & AddRow ( const string & str );
            CEmpty * MakeCopy() const override;
            CImage(const CImage & other);
            CImage(){}
            CImage & operator=(const CImage & other){return *this;}

    };

    CImage::CImage(const CImage & other)
    {
        this ->m_Content = other.m_Content;
    }
    unsigned int CImage::GetWidth()
    {
        if(m_Content.empty())
        {
            return 0;
        }
        return m_Content[0].size();   
    }

    CImage & CImage::AddRow ( const string & str )
    {
        m_Content.push_back(str);
        return *this;
    }

    void CImage::PrintLine( ostream & os, int lineNum, int w, int h )
    {
        if(lineNum < (int)(h - m_Content.size())/2  || lineNum >= (int)(m_Content.size() + (h-m_Content.size())/2) ){
            for(int i = 0;  i< w; i++)
            {
                os << " ";
            }
        }
        else
        {
            int spaces =(w - m_Content[(int) (lineNum - (h - m_Content.size())/2  )].size());
            for( int i = 0; i < spaces/2; ++i)
            {
                os << " ";
            }   
            os << m_Content[(int) (lineNum - (h - m_Content.size())/2 )];
            if(spaces%2!=0)
            {
                spaces++;
            }
            for( int i = 0; i < spaces/2; ++i)
            {
                os << " ";
            }   
        }
    }

    CEmpty * CImage::MakeCopy() const
    {
        return new CImage(*this);
    }
//------------------CText--------------------------------------------------
    class CText : public CContentCell
    {
        public:
            static const int ALIGN_LEFT  = 0;
            static const int ALIGN_RIGHT = 1;
            CText      (const char * str, int align); 
            void SetText    (const char * str);
            void PrintLine( ostream & os, int lineNum, int w, int h ) override;
            unsigned int GetWidth() override;
            CEmpty * MakeCopy() const override;
            CText(const CText & other);
            CText & operator=(const CText & other);

        private:
            unsigned int m_Width;
            int m_Align;
    };

    CText::CText(const char * str, int align):  m_Width(0),
                                                m_Align(align)
                                               
    {
        if(str!=nullptr)
        {
            SetText(str);
        }
    }

    CText::CText(const CText & other)
    {
        this -> m_Align = other.m_Align;
        this -> m_Width = other.m_Width;
        this -> m_Content = other.m_Content;
    }

    CText & CText::operator=(const CText & other)
    {
        if(this != &other){
            this -> m_Align = other.m_Align;
            this -> m_Width = other.m_Width;
            this -> m_Content = other.m_Content;
        }
       
        return *this;
    }

    unsigned int CText::GetWidth()
    {
        return m_Width;
    }

    void CText::SetText(const char * str)
    {
        m_Content.clear();
        stringstream st;
        st<<str;
        string line;
        m_Width = 0;
        while( getline(st, line) )
        {
            if((unsigned)m_Width < line.size()){m_Width = line.size();}
            m_Content.push_back(line);
        }
    }

 
    void CText::PrintLine( ostream & os, int lineNum, int w, int h )
    {
       
        if(lineNum < (int)m_Content.size()){
             if(m_Align == ALIGN_RIGHT)
                    {
                        for(int i = 0; i < (w -(int) m_Content[lineNum].size()); ++i)
                        {
                            os << " ";
                        }   
                    }



            os << m_Content[lineNum];



             if(m_Align == ALIGN_LEFT)
                    {
                        for(int i = 0; i < (w - (int)m_Content[lineNum].size()); ++i)
                        {
                            os << " ";
                       

                        }   
                    }
        }
        else
        {
             for(int i = 0; i < w; ++i)
                {
                    os << " ";
                  
                }
        }
        
       
    }

    CEmpty * CText::MakeCopy() const
    {
        return new CText(*this);
    }
//------------------CTable-------------------------------------------------
    class CTable : public CEmpty
    {
        public:
            CTable                          (int rows, int cols);
            ~CTable();
            CEmpty & GetCell                (int x, int y);
            void SetCell                    (int x, int y, const CEmpty & newCell); 
            friend ostream & operator <<    (ostream & os, CTable & table); 
            CTable (const CTable & other);
            CTable & operator=(const CTable &);
            
            void PrintLine( ostream & os, int lineNum, int w, int h ) override;
            unsigned int GetWidth() override;
            unsigned int GetHeight() override;
            CEmpty * MakeCopy() const override;

        private:
            int                            m_Rows;
            int                            m_Cols;
            vector<CEmpty *>               m_Cells;
            vector<int>                    m_Widths;
            vector<int>                    m_Heights;
            int FindMaxHeight               ( int rowPos );
            int FindMaxWidth                ( int colPos );
            CEmpty * trash;
            vector<int> m_Borders;
    };


     //-------------SECOND
          unsigned int CTable::GetWidth()
          {
               unsigned int table_Width = 1;
               for(int i = 0; i < m_Cols; i++)
               {
                    table_Width += FindMaxWidth(i) + 1;
               }
               return table_Width;
          }


          unsigned int CTable::GetHeight()
          {
               unsigned int table_Height = 1;
               for(int i = 0; i < m_Rows; i++)
               {
                    table_Height += FindMaxHeight(i) + 1;
               }
               return table_Height;
          }

     

          CEmpty * CTable::MakeCopy() const
          {
               return new CTable(*this);
          }


          void CTable::PrintLine( ostream & os, int lineNum, int w, int h ) 
          {
               if(lineNum == 0)
               {
                    m_Widths.clear();
                    m_Heights.clear();
                    m_Borders.clear();
                    for(int i = 0; i < m_Cols; i++)
                    {
                         m_Widths.push_back(FindMaxWidth(i));
                    }
                    m_Borders.push_back(0); 
                    for(int i = 0; i < m_Rows; i++)
                    {
                         m_Heights.push_back(FindMaxHeight(i));     
                         m_Borders.push_back(m_Borders[i] + m_Heights[i] + 1);
                        // cout << m_Borders[i+1] << endl;
                    }
               }
     
               if(lineNum < (int)GetHeight())
               {
                    int rowNum = 0;
                    while(lineNum > m_Borders[rowNum])
                    {     
                         rowNum++;
                    }
                    if(lineNum == m_Borders[rowNum] )
                    {
                         for(int i = 0; i < m_Cols; ++i)
                         {
                              os << '+';
                              for(int j = 0; j < m_Widths[i]; ++j)
                              {
                                   os << '-';
                              }
                         }
                         os << '+';
                              for(int i = 0; i < (w - (int)GetWidth()); ++i)
                              {
                                   os << " ";
                              }   
                    }
                    else
                    {
                         int curRowLineNum = lineNum - m_Borders[rowNum-1] - 1 ;
                        // cout <<  rowNum - 1 << '\t' << lineNum - m_Borders[rowNum-1] - 1 <<   endl;

                         os << "|";
                         for(int j = 0; j < m_Cols; j++){
                              m_Cells[(rowNum - 1) * m_Cols + j]->PrintLine(os, curRowLineNum , m_Widths[j], m_Heights[rowNum - 1]);
                              os << "|";
                         }    

                         for(int i = 0; i < (w - (int)GetWidth()); ++i)
                         {
                              os << " ";
                         }   
                    }
                    
                   
               }

               else{
                    for(int i = 0; i < w ; ++i){
                         os << " ";
                    }
               }    
                    
     
          }




    CTable::CTable(int rows, int cols): m_Rows(rows),
                                        m_Cols(cols)
    {
        for(int i = 0; i < m_Rows * m_Cols; ++i)
        {
            m_Cells.push_back(new CEmpty());
        }
        trash = new CEmpty();
    }

    CTable::~CTable()
    {
        for(int i = 0; i < m_Rows * m_Cols; ++i )
        {
            delete m_Cells[i];
        }
        delete trash;
    }
    
    CTable::CTable (const CTable & other){
        this -> m_Rows = other.m_Rows;
        this -> m_Cols = other.m_Cols;
        for(int i = 0; i < m_Rows* m_Cols; i++){
            CEmpty * tmp = (other.m_Cells[i]->MakeCopy());
           this -> m_Cells.push_back(tmp);
        }
        this -> trash = other.trash->MakeCopy();
    }
    
    CTable &CTable::operator=(const CTable & other)
    {
       
        if(this != &other){
             for(int i = 0; i < m_Rows * m_Cols; ++i )
                {
                    delete m_Cells[i];
                }
         
            this -> m_Cells.clear();
            this -> m_Rows = other.m_Rows;
            this -> m_Cols = other.m_Cols;
            for(int i = 0; i < m_Rows* m_Cols; i++){
                CEmpty * tmp = (other.m_Cells[i]->MakeCopy());
            this -> m_Cells.push_back(tmp);
            }
           
        }
        return *this;
    }

    int CTable::FindMaxWidth( int colPos )
    {
        unsigned int max = 0;
        for(int i = colPos; i < m_Rows * m_Cols; i+=m_Cols )
        {
            if(m_Cells[i]->GetWidth() > max)
            {
                max = m_Cells[i]->GetWidth();
            }
        }
        return max;
    }

    int CTable::FindMaxHeight( int rowPos )
    {
         unsigned int max = 0;
        for(int i = rowPos * m_Cols; i <rowPos * m_Cols +m_Cols; i++ )
        {
            if(m_Cells[i]->GetHeight() > max)
            {
                max = m_Cells[i]->GetHeight();
            }
        }
        return max;
    }

    CEmpty & CTable::GetCell(int x, int y)
    {
         if(x < (int)m_Cells.size() / m_Cols && y < (int)m_Cells.size()/m_Rows)
        {
            return *m_Cells[x*m_Cols + y];
        }
        return *trash;
    }

    void CTable::SetCell(int x, int y, const CEmpty & newCell)
    {
        if(x < (int)m_Cells.size() / m_Cols && y < (int)m_Cells.size()/m_Rows)
        {
       
                   
          
           
        CEmpty * tmp = newCell.MakeCopy();
        delete m_Cells[x*m_Cols + y];
        m_Cells[x*m_Cols + y] = tmp;
        }
        
    }

     ostream & operator << (ostream & os, CTable & table)
    {   
        table.m_Widths.clear();
        table.m_Heights.clear();

        for(int i = 0; i < table.m_Cols; i++)
        {
            table.m_Widths.push_back(table.FindMaxWidth(i));
        }
    
        for(int i = 0; i < table.m_Rows; i++)
        {
            table.m_Heights.push_back(table.FindMaxHeight(i));
           
        }


       
        for(int i = 0; i < table.m_Rows; i++)
        {
            
            for(int t = 0; t < table.m_Cols; t++)
            {
                os << '+';
                for(int k = 0; k < table.m_Widths[t] ; k++){
                    os << '-';
                }      
            }
            os << '+'<<endl;
            for(int t = 0; t < table.m_Heights[i]; t++){
                os << "|";
                for(int j = 0; j < table.m_Cols; j++){
                    table.m_Cells[i * table.m_Cols + j]->PrintLine(os, t , table.m_Widths[j], table.m_Heights[i]);
                    os << "|";
                }
               
                os << endl;

            }
        }
        for(int t = 0; t < table.m_Cols; t++)
            {
                os << '+';
                for(int k = 0; k < table.m_Widths[t] ; k++){
                    os << '-';
                }      
            }
            os << '+'<<endl;
        return os;
    }

#ifndef __PROGTEST__
int main ( void )
{
  ostringstream oss;
   CTable t0 ( 3, 2 );
   t0 . SetCell ( 0, 0, CText ( "Hello,\n"
        "Hello Kitty", CText::ALIGN_LEFT ) );
   t0 . SetCell ( 1, 0, CText ( "Lorem ipsum dolor sit amet", CText::ALIGN_LEFT ) );
   t0 . SetCell ( 2, 0, CText ( "Bye,\n"
        "Hello Kitty", CText::ALIGN_RIGHT ) );
   t0 . SetCell ( 1, 1, CImage ()
          . AddRow ( "###                   " )
          . AddRow ( "#  #                  " )
          . AddRow ( "#  # # ##   ###    ###" )
          . AddRow ( "###  ##    #   #  #  #" )
          . AddRow ( "#    #     #   #  #  #" )
          . AddRow ( "#    #     #   #  #  #" )
          . AddRow ( "#    #      ###    ###" )
          . AddRow ( "                     #" )
          . AddRow ( "                   ## " )
          . AddRow ( "                      " )
          . AddRow ( " #    ###   ###   #   " )
          . AddRow ( "###  #   # #     ###  " )
          . AddRow ( " #   #####  ###   #   " )
          . AddRow ( " #   #         #  #   " )
          . AddRow ( "  ##  ###   ###    ## " ) );
   t0 . SetCell ( 2, 1, CEmpty () );
   oss . str ("");
   oss . clear ();
   oss << t0;
   assert ( oss . str () ==
        "+--------------------------+----------------------+\n"
        "|Hello,                    |                      |\n"
        "|Hello Kitty               |                      |\n"
        "+--------------------------+----------------------+\n"
        "|Lorem ipsum dolor sit amet|###                   |\n"
        "|                          |#  #                  |\n"
        "|                          |#  # # ##   ###    ###|\n"
        "|                          |###  ##    #   #  #  #|\n"
        "|                          |#    #     #   #  #  #|\n"
        "|                          |#    #     #   #  #  #|\n"
        "|                          |#    #      ###    ###|\n"
        "|                          |                     #|\n"
        "|                          |                   ## |\n"
        "|                          |                      |\n"
        "|                          | #    ###   ###   #   |\n"
        "|                          |###  #   # #     ###  |\n"
        "|                          | #   #####  ###   #   |\n"
        "|                          | #   #         #  #   |\n"
        "|                          |  ##  ###   ###    ## |\n"
        "+--------------------------+----------------------+\n"
        "|                      Bye,|                      |\n"
        "|               Hello Kitty|                      |\n"
        "+--------------------------+----------------------+\n" );
   t0 . SetCell ( 0, 1, t0 . GetCell ( 1, 1 ) );
   t0 . SetCell ( 2, 1, CImage ()
          . AddRow ( "*****   *      *  *      ******* ******  *" )
          . AddRow ( "*    *  *      *  *      *            *  *" )
          . AddRow ( "*    *  *      *  *      *           *   *" )
          . AddRow ( "*    *  *      *  *      *****      *    *" )
          . AddRow ( "****    *      *  *      *         *     *" )
          . AddRow ( "*  *    *      *  *      *        *       " )
          . AddRow ( "*   *   *      *  *      *       *       *" )
          . AddRow ( "*    *    *****   ****** ******* ******  *" ) );
   dynamic_cast<CText &> ( t0 . GetCell ( 1, 0 ) ) . SetText ( "Lorem ipsum dolor sit amet,\n"
        "consectetur adipiscing\n"
        "elit. Curabitur scelerisque\n"
        "lorem vitae lectus cursus,\n"
        "vitae porta ante placerat. Class aptent taciti\n"
        "sociosqu ad litora\n"
        "torquent per\n"
        "conubia nostra,\n"
        "per inceptos himenaeos.\n"
        "\n"
        "Donec tincidunt augue\n"
        "sit amet metus\n"
        "pretium volutpat.\n"
        "Donec faucibus,\n"
        "ante sit amet\n"
        "luctus posuere,\n"
        "mauris tellus" );
   oss . str ("");
   oss . clear ();
   oss << t0;
  assert ( oss . str () ==
        "+----------------------------------------------+------------------------------------------+\n"
        "|Hello,                                        |          ###                             |\n"
        "|Hello Kitty                                   |          #  #                            |\n"
        "|                                              |          #  # # ##   ###    ###          |\n"
        "|                                              |          ###  ##    #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #      ###    ###          |\n"
        "|                                              |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|                                              |                                          |\n"
        "|                                              |           #    ###   ###   #             |\n"
        "|                                              |          ###  #   # #     ###            |\n"
        "|                                              |           #   #####  ###   #             |\n"
        "|                                              |           #   #         #  #             |\n"
        "|                                              |            ##  ###   ###    ##           |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                   |                                          |\n"
        "|consectetur adipiscing                        |          ###                             |\n"
        "|elit. Curabitur scelerisque                   |          #  #                            |\n"
        "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
        "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
        "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
        "|torquent per                                  |          #    #     #   #  #  #          |\n"
        "|conubia nostra,                               |          #    #      ###    ###          |\n"
        "|per inceptos himenaeos.                       |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|Donec tincidunt augue                         |                                          |\n"
        "|sit amet metus                                |           #    ###   ###   #             |\n"
        "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
        "|Donec faucibus,                               |           #   #####  ###   #             |\n"
        "|ante sit amet                                 |           #   #         #  #             |\n"
        "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
        "|mauris tellus                                 |                                          |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                              |*    *  *      *  *      *           *   *|\n"
        "|                                              |*    *  *      *  *      *****      *    *|\n"
        "|                                              |****    *      *  *      *         *     *|\n"
        "|                                              |*  *    *      *  *      *        *       |\n"
        "|                                              |*   *   *      *  *      *       *       *|\n"
        "|                                              |*    *    *****   ****** ******* ******  *|\n"
        "+----------------------------------------------+------------------------------------------+\n" );
   CTable t1 ( t0 );
   t1 . SetCell ( 1, 0, CEmpty () );
   t1 . SetCell ( 1, 1, CEmpty () );
   oss . str ("");
   oss . clear ();
   oss << t0;
   assert ( oss . str () ==
        "+----------------------------------------------+------------------------------------------+\n"
        "|Hello,                                        |          ###                             |\n"
        "|Hello Kitty                                   |          #  #                            |\n"
        "|                                              |          #  # # ##   ###    ###          |\n"
        "|                                              |          ###  ##    #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #      ###    ###          |\n"
        "|                                              |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|                                              |                                          |\n"
        "|                                              |           #    ###   ###   #             |\n"
        "|                                              |          ###  #   # #     ###            |\n"
        "|                                              |           #   #####  ###   #             |\n"
        "|                                              |           #   #         #  #             |\n"
        "|                                              |            ##  ###   ###    ##           |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                   |                                          |\n"
        "|consectetur adipiscing                        |          ###                             |\n"
        "|elit. Curabitur scelerisque                   |          #  #                            |\n"
        "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
        "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
        "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
        "|torquent per                                  |          #    #     #   #  #  #          |\n"
        "|conubia nostra,                               |          #    #      ###    ###          |\n"
        "|per inceptos himenaeos.                       |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|Donec tincidunt augue                         |                                          |\n"
        "|sit amet metus                                |           #    ###   ###   #             |\n"
        "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
        "|Donec faucibus,                               |           #   #####  ###   #             |\n"
        "|ante sit amet                                 |           #   #         #  #             |\n"
        "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
        "|mauris tellus                                 |                                          |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                              |*    *  *      *  *      *           *   *|\n"
        "|                                              |*    *  *      *  *      *****      *    *|\n"
        "|                                              |****    *      *  *      *         *     *|\n"
        "|                                              |*  *    *      *  *      *        *       |\n"
        "|                                              |*   *   *      *  *      *       *       *|\n"
        "|                                              |*    *    *****   ****** ******* ******  *|\n"
        "+----------------------------------------------+------------------------------------------+\n" );
   oss . str ("");
   oss . clear ();
   oss << t1;
   assert ( oss . str () ==
        "+-----------+------------------------------------------+\n"
        "|Hello,     |          ###                             |\n"
        "|Hello Kitty|          #  #                            |\n"
        "|           |          #  # # ##   ###    ###          |\n"
        "|           |          ###  ##    #   #  #  #          |\n"
        "|           |          #    #     #   #  #  #          |\n"
        "|           |          #    #     #   #  #  #          |\n"
        "|           |          #    #      ###    ###          |\n"
        "|           |                               #          |\n"
        "|           |                             ##           |\n"
        "|           |                                          |\n"
        "|           |           #    ###   ###   #             |\n"
        "|           |          ###  #   # #     ###            |\n"
        "|           |           #   #####  ###   #             |\n"
        "|           |           #   #         #  #             |\n"
        "|           |            ##  ###   ###    ##           |\n"
        "+-----------+------------------------------------------+\n"
        "+-----------+------------------------------------------+\n"
        "|       Bye,|*****   *      *  *      ******* ******  *|\n"
        "|Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|           |*    *  *      *  *      *           *   *|\n"
        "|           |*    *  *      *  *      *****      *    *|\n"
        "|           |****    *      *  *      *         *     *|\n"
        "|           |*  *    *      *  *      *        *       |\n"
        "|           |*   *   *      *  *      *       *       *|\n"
        "|           |*    *    *****   ****** ******* ******  *|\n"
        "+-----------+------------------------------------------+\n" );
   t1 = t0;
   t1 . SetCell ( 0, 0, CEmpty () );
   t1 . SetCell ( 1, 1, CImage ()
          . AddRow ( "  ********                    " )
          . AddRow ( " **********                   " )
          . AddRow ( "**        **                  " )
          . AddRow ( "**             **        **   " )
          . AddRow ( "**             **        **   " )
          . AddRow ( "***         ********  ********" )
          . AddRow ( "****        ********  ********" )
          . AddRow ( "****           **        **   " )
          . AddRow ( "****           **        **   " )
          . AddRow ( "****      **                  " )
          . AddRow ( " **********                   " )
          . AddRow ( "  ********                    " ) );
   oss . str ("");
   oss . clear ();
   oss << t0;
   assert ( oss . str () ==
        "+----------------------------------------------+------------------------------------------+\n"
        "|Hello,                                        |          ###                             |\n"
        "|Hello Kitty                                   |          #  #                            |\n"
        "|                                              |          #  # # ##   ###    ###          |\n"
        "|                                              |          ###  ##    #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #      ###    ###          |\n"
        "|                                              |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|                                              |                                          |\n"
        "|                                              |           #    ###   ###   #             |\n"
        "|                                              |          ###  #   # #     ###            |\n"
        "|                                              |           #   #####  ###   #             |\n"
        "|                                              |           #   #         #  #             |\n"
        "|                                              |            ##  ###   ###    ##           |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                   |                                          |\n"
        "|consectetur adipiscing                        |          ###                             |\n"
        "|elit. Curabitur scelerisque                   |          #  #                            |\n"
        "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
        "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
        "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
        "|torquent per                                  |          #    #     #   #  #  #          |\n"
        "|conubia nostra,                               |          #    #      ###    ###          |\n"
        "|per inceptos himenaeos.                       |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|Donec tincidunt augue                         |                                          |\n"
        "|sit amet metus                                |           #    ###   ###   #             |\n"
        "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
        "|Donec faucibus,                               |           #   #####  ###   #             |\n"
        "|ante sit amet                                 |           #   #         #  #             |\n"
        "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
        "|mauris tellus                                 |                                          |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                              |*    *  *      *  *      *           *   *|\n"
        "|                                              |*    *  *      *  *      *****      *    *|\n"
        "|                                              |****    *      *  *      *         *     *|\n"
        "|                                              |*  *    *      *  *      *        *       |\n"
        "|                                              |*   *   *      *  *      *       *       *|\n"
        "|                                              |*    *    *****   ****** ******* ******  *|\n"
        "+----------------------------------------------+------------------------------------------+\n" );
   oss . str ("");
   oss . clear ();
   oss << t1;
   assert ( oss . str () ==
        "+----------------------------------------------+------------------------------------------+\n"
        "|                                              |          ###                             |\n"
        "|                                              |          #  #                            |\n"
        "|                                              |          #  # # ##   ###    ###          |\n"
        "|                                              |          ###  ##    #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #      ###    ###          |\n"
        "|                                              |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|                                              |                                          |\n"
        "|                                              |           #    ###   ###   #             |\n"
        "|                                              |          ###  #   # #     ###            |\n"
        "|                                              |           #   #####  ###   #             |\n"
        "|                                              |           #   #         #  #             |\n"
        "|                                              |            ##  ###   ###    ##           |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                   |                                          |\n"
        "|consectetur adipiscing                        |                                          |\n"
        "|elit. Curabitur scelerisque                   |        ********                          |\n"
        "|lorem vitae lectus cursus,                    |       **********                         |\n"
        "|vitae porta ante placerat. Class aptent taciti|      **        **                        |\n"
        "|sociosqu ad litora                            |      **             **        **         |\n"
        "|torquent per                                  |      **             **        **         |\n"
        "|conubia nostra,                               |      ***         ********  ********      |\n"
        "|per inceptos himenaeos.                       |      ****        ********  ********      |\n"
        "|                                              |      ****           **        **         |\n"
        "|Donec tincidunt augue                         |      ****           **        **         |\n"
        "|sit amet metus                                |      ****      **                        |\n"
        "|pretium volutpat.                             |       **********                         |\n"
        "|Donec faucibus,                               |        ********                          |\n"
        "|ante sit amet                                 |                                          |\n"
        "|luctus posuere,                               |                                          |\n"
        "|mauris tellus                                 |                                          |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                              |*    *  *      *  *      *           *   *|\n"
        "|                                              |*    *  *      *  *      *****      *    *|\n"
        "|                                              |****    *      *  *      *         *     *|\n"
        "|                                              |*  *    *      *  *      *        *       |\n"
        "|                                              |*   *   *      *  *      *       *       *|\n"
        "|                                              |*    *    *****   ****** ******* ******  *|\n"
        "+----------------------------------------------+------------------------------------------+\n" );
   CTable t2 ( 2, 2 );
   t2 . SetCell ( 0, 0, CText ( "OOP", CText::ALIGN_LEFT ) );
   t2 . SetCell ( 0, 1, CText ( "Encapsulation", CText::ALIGN_LEFT ) );
   t2 . SetCell ( 1, 0, CText ( "Polymorphism", CText::ALIGN_LEFT ) );
   t2 . SetCell ( 1, 1, CText ( "Inheritance", CText::ALIGN_LEFT ) );
   oss . str ("");
   oss . clear ();
   oss << t2;
   assert ( oss . str () ==
        "+------------+-------------+\n"
        "|OOP         |Encapsulation|\n"
        "+------------+-------------+\n"
        "|Polymorphism|Inheritance  |\n"
        "+------------+-------------+\n" );
   t1 . SetCell ( 0, 0, t2 );
   dynamic_cast<CText &> ( t2 . GetCell ( 0, 0 ) ) . SetText ( "Object Oriented Programming" );
   oss . str ("");
   oss . clear ();
   oss << t2;
   assert ( oss . str () ==
        "+---------------------------+-------------+\n"
        "|Object Oriented Programming|Encapsulation|\n"
        "+---------------------------+-------------+\n"
        "|Polymorphism               |Inheritance  |\n"
        "+---------------------------+-------------+\n" );
   oss . str ("");
   oss . clear ();
  //cout << t1;
     oss << t1;
  assert ( oss . str () ==
        "+----------------------------------------------+------------------------------------------+\n"
        "|+------------+-------------+                  |          ###                             |\n"
        "||OOP         |Encapsulation|                  |          #  #                            |\n"
        "|+------------+-------------+                  |          #  # # ##   ###    ###          |\n"
        "||Polymorphism|Inheritance  |                  |          ###  ##    #   #  #  #          |\n"
        "|+------------+-------------+                  |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #     #   #  #  #          |\n"
        "|                                              |          #    #      ###    ###          |\n"
        "|                                              |                               #          |\n"
        "|                                              |                             ##           |\n"
        "|                                              |                                          |\n"
        "|                                              |           #    ###   ###   #             |\n"
        "|                                              |          ###  #   # #     ###            |\n"
        "|                                              |           #   #####  ###   #             |\n"
        "|                                              |           #   #         #  #             |\n"
        "|                                              |            ##  ###   ###    ##           |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                   |                                          |\n"
        "|consectetur adipiscing                        |                                          |\n"
        "|elit. Curabitur scelerisque                   |        ********                          |\n"
        "|lorem vitae lectus cursus,                    |       **********                         |\n"
        "|vitae porta ante placerat. Class aptent taciti|      **        **                        |\n"
        "|sociosqu ad litora                            |      **             **        **         |\n"
        "|torquent per                                  |      **             **        **         |\n"
        "|conubia nostra,                               |      ***         ********  ********      |\n"
        "|per inceptos himenaeos.                       |      ****        ********  ********      |\n"
        "|                                              |      ****           **        **         |\n"
        "|Donec tincidunt augue                         |      ****           **        **         |\n"
        "|sit amet metus                                |      ****      **                        |\n"
        "|pretium volutpat.                             |       **********                         |\n"
        "|Donec faucibus,                               |        ********                          |\n"
        "|ante sit amet                                 |                                          |\n"
        "|luctus posuere,                               |                                          |\n"
        "|mauris tellus                                 |                                          |\n"
        "+----------------------------------------------+------------------------------------------+\n"
        "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                              |*    *  *      *  *      *           *   *|\n"
        "|                                              |*    *  *      *  *      *****      *    *|\n"
        "|                                              |****    *      *  *      *         *     *|\n"
        "|                                              |*  *    *      *  *      *        *       |\n"
        "|                                              |*   *   *      *  *      *       *       *|\n"
        "|                                              |*    *    *****   ****** ******* ******  *|\n"
        "+----------------------------------------------+------------------------------------------+\n" );
     t1 . SetCell ( 0, 0, t1 );
   oss . str ("");
   oss . clear ();
 //cout << t1;
   oss << t1;
   assert ( oss . str () ==
        "+-------------------------------------------------------------------------------------------+------------------------------------------+\n"
        "|+----------------------------------------------+------------------------------------------+|                                          |\n"
        "||+------------+-------------+                  |          ###                             ||                                          |\n"
        "|||OOP         |Encapsulation|                  |          #  #                            ||                                          |\n"
        "||+------------+-------------+                  |          #  # # ##   ###    ###          ||                                          |\n"
        "|||Polymorphism|Inheritance  |                  |          ###  ##    #   #  #  #          ||                                          |\n"
        "||+------------+-------------+                  |          #    #     #   #  #  #          ||                                          |\n"
        "||                                              |          #    #     #   #  #  #          ||                                          |\n"
        "||                                              |          #    #      ###    ###          ||                                          |\n"
        "||                                              |                               #          ||                                          |\n"
        "||                                              |                             ##           ||                                          |\n"
        "||                                              |                                          ||                                          |\n"
        "||                                              |           #    ###   ###   #             ||                                          |\n"
        "||                                              |          ###  #   # #     ###            ||                                          |\n"
        "||                                              |           #   #####  ###   #             ||                                          |\n"
        "||                                              |           #   #         #  #             ||          ###                             |\n"
        "||                                              |            ##  ###   ###    ##           ||          #  #                            |\n"
        "|+----------------------------------------------+------------------------------------------+|          #  # # ##   ###    ###          |\n"
        "||Lorem ipsum dolor sit amet,                   |                                          ||          ###  ##    #   #  #  #          |\n"
        "||consectetur adipiscing                        |                                          ||          #    #     #   #  #  #          |\n"
        "||elit. Curabitur scelerisque                   |        ********                          ||          #    #     #   #  #  #          |\n"
        "||lorem vitae lectus cursus,                    |       **********                         ||          #    #      ###    ###          |\n"
        "||vitae porta ante placerat. Class aptent taciti|      **        **                        ||                               #          |\n"
        "||sociosqu ad litora                            |      **             **        **         ||                             ##           |\n"
        "||torquent per                                  |      **             **        **         ||                                          |\n"
        "||conubia nostra,                               |      ***         ********  ********      ||           #    ###   ###   #             |\n"
        "||per inceptos himenaeos.                       |      ****        ********  ********      ||          ###  #   # #     ###            |\n"
        "||                                              |      ****           **        **         ||           #   #####  ###   #             |\n"
        "||Donec tincidunt augue                         |      ****           **        **         ||           #   #         #  #             |\n"
        "||sit amet metus                                |      ****      **                        ||            ##  ###   ###    ##           |\n"
        "||pretium volutpat.                             |       **********                         ||                                          |\n"
        "||Donec faucibus,                               |        ********                          ||                                          |\n"
        "||ante sit amet                                 |                                          ||                                          |\n"
        "||luctus posuere,                               |                                          ||                                          |\n"
        "||mauris tellus                                 |                                          ||                                          |\n"
        "|+----------------------------------------------+------------------------------------------+|                                          |\n"
        "||                                          Bye,|*****   *      *  *      ******* ******  *||                                          |\n"
        "||                                   Hello Kitty|*    *  *      *  *      *            *  *||                                          |\n"
        "||                                              |*    *  *      *  *      *           *   *||                                          |\n"
        "||                                              |*    *  *      *  *      *****      *    *||                                          |\n"
        "||                                              |****    *      *  *      *         *     *||                                          |\n"
        "||                                              |*  *    *      *  *      *        *       ||                                          |\n"
        "||                                              |*   *   *      *  *      *       *       *||                                          |\n"
        "||                                              |*    *    *****   ****** ******* ******  *||                                          |\n"
        "|+----------------------------------------------+------------------------------------------+|                                          |\n"
        "+-------------------------------------------------------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                                                                |                                          |\n"
        "|consectetur adipiscing                                                                     |                                          |\n"
        "|elit. Curabitur scelerisque                                                                |        ********                          |\n"
        "|lorem vitae lectus cursus,                                                                 |       **********                         |\n"
        "|vitae porta ante placerat. Class aptent taciti                                             |      **        **                        |\n"
        "|sociosqu ad litora                                                                         |      **             **        **         |\n"
        "|torquent per                                                                               |      **             **        **         |\n"
        "|conubia nostra,                                                                            |      ***         ********  ********      |\n"
        "|per inceptos himenaeos.                                                                    |      ****        ********  ********      |\n"
        "|                                                                                           |      ****           **        **         |\n"
        "|Donec tincidunt augue                                                                      |      ****           **        **         |\n"
        "|sit amet metus                                                                             |      ****      **                        |\n"
        "|pretium volutpat.                                                                          |       **********                         |\n"
        "|Donec faucibus,                                                                            |        ********                          |\n"
        "|ante sit amet                                                                              |                                          |\n"
        "|luctus posuere,                                                                            |                                          |\n"
        "|mauris tellus                                                                              |                                          |\n"
        "+-------------------------------------------------------------------------------------------+------------------------------------------+\n"
        "|                                                                                       Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                                                                Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                                                                           |*    *  *      *  *      *           *   *|\n"
        "|                                                                                           |*    *  *      *  *      *****      *    *|\n"
        "|                                                                                           |****    *      *  *      *         *     *|\n"
        "|                                                                                           |*  *    *      *  *      *        *       |\n"
        "|                                                                                           |*   *   *      *  *      *       *       *|\n"
        "|                                                                                           |*    *    *****   ****** ******* ******  *|\n"
        "+-------------------------------------------------------------------------------------------+------------------------------------------+\n" );
   t1 . SetCell ( 0, 0, t1 );
   oss . str ("");
   oss . clear ();
  // cout << t1;
   oss << t1;
   assert ( oss . str () ==
        "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n"
        "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
        "||+----------------------------------------------+------------------------------------------+|                                          ||                                          |\n"
        "|||+------------+-------------+                  |          ###                             ||                                          ||                                          |\n"
        "||||OOP         |Encapsulation|                  |          #  #                            ||                                          ||                                          |\n"
        "|||+------------+-------------+                  |          #  # # ##   ###    ###          ||                                          ||                                          |\n"
        "||||Polymorphism|Inheritance  |                  |          ###  ##    #   #  #  #          ||                                          ||                                          |\n"
        "|||+------------+-------------+                  |          #    #     #   #  #  #          ||                                          ||                                          |\n"
        "|||                                              |          #    #     #   #  #  #          ||                                          ||                                          |\n"
        "|||                                              |          #    #      ###    ###          ||                                          ||                                          |\n"
        "|||                                              |                               #          ||                                          ||                                          |\n"
        "|||                                              |                             ##           ||                                          ||                                          |\n"
        "|||                                              |                                          ||                                          ||                                          |\n"
        "|||                                              |           #    ###   ###   #             ||                                          ||                                          |\n"
        "|||                                              |          ###  #   # #     ###            ||                                          ||                                          |\n"
        "|||                                              |           #   #####  ###   #             ||                                          ||                                          |\n"
        "|||                                              |           #   #         #  #             ||          ###                             ||                                          |\n"
        "|||                                              |            ##  ###   ###    ##           ||          #  #                            ||                                          |\n"
        "||+----------------------------------------------+------------------------------------------+|          #  # # ##   ###    ###          ||                                          |\n"
        "|||Lorem ipsum dolor sit amet,                   |                                          ||          ###  ##    #   #  #  #          ||                                          |\n"
        "|||consectetur adipiscing                        |                                          ||          #    #     #   #  #  #          ||                                          |\n"
        "|||elit. Curabitur scelerisque                   |        ********                          ||          #    #     #   #  #  #          ||                                          |\n"
        "|||lorem vitae lectus cursus,                    |       **********                         ||          #    #      ###    ###          ||                                          |\n"
        "|||vitae porta ante placerat. Class aptent taciti|      **        **                        ||                               #          ||                                          |\n"
        "|||sociosqu ad litora                            |      **             **        **         ||                             ##           ||                                          |\n"
        "|||torquent per                                  |      **             **        **         ||                                          ||                                          |\n"
        "|||conubia nostra,                               |      ***         ********  ********      ||           #    ###   ###   #             ||                                          |\n"
        "|||per inceptos himenaeos.                       |      ****        ********  ********      ||          ###  #   # #     ###            ||                                          |\n"
        "|||                                              |      ****           **        **         ||           #   #####  ###   #             ||                                          |\n"
        "|||Donec tincidunt augue                         |      ****           **        **         ||           #   #         #  #             ||                                          |\n"
        "|||sit amet metus                                |      ****      **                        ||            ##  ###   ###    ##           ||          ###                             |\n"
        "|||pretium volutpat.                             |       **********                         ||                                          ||          #  #                            |\n"
        "|||Donec faucibus,                               |        ********                          ||                                          ||          #  # # ##   ###    ###          |\n"
        "|||ante sit amet                                 |                                          ||                                          ||          ###  ##    #   #  #  #          |\n"
        "|||luctus posuere,                               |                                          ||                                          ||          #    #     #   #  #  #          |\n"
        "|||mauris tellus                                 |                                          ||                                          ||          #    #     #   #  #  #          |\n"
        "||+----------------------------------------------+------------------------------------------+|                                          ||          #    #      ###    ###          |\n"
        "|||                                          Bye,|*****   *      *  *      ******* ******  *||                                          ||                               #          |\n"
        "|||                                   Hello Kitty|*    *  *      *  *      *            *  *||                                          ||                             ##           |\n"
        "|||                                              |*    *  *      *  *      *           *   *||                                          ||                                          |\n"
        "|||                                              |*    *  *      *  *      *****      *    *||                                          ||           #    ###   ###   #             |\n"
        "|||                                              |****    *      *  *      *         *     *||                                          ||          ###  #   # #     ###            |\n"
        "|||                                              |*  *    *      *  *      *        *       ||                                          ||           #   #####  ###   #             |\n"
        "|||                                              |*   *   *      *  *      *       *       *||                                          ||           #   #         #  #             |\n"
        "|||                                              |*    *    *****   ****** ******* ******  *||                                          ||            ##  ###   ###    ##           |\n"
        "||+----------------------------------------------+------------------------------------------+|                                          ||                                          |\n"
        "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
        "||Lorem ipsum dolor sit amet,                                                                |                                          ||                                          |\n"
        "||consectetur adipiscing                                                                     |                                          ||                                          |\n"
        "||elit. Curabitur scelerisque                                                                |        ********                          ||                                          |\n"
        "||lorem vitae lectus cursus,                                                                 |       **********                         ||                                          |\n"
        "||vitae porta ante placerat. Class aptent taciti                                             |      **        **                        ||                                          |\n"
        "||sociosqu ad litora                                                                         |      **             **        **         ||                                          |\n"
        "||torquent per                                                                               |      **             **        **         ||                                          |\n"
        "||conubia nostra,                                                                            |      ***         ********  ********      ||                                          |\n"
        "||per inceptos himenaeos.                                                                    |      ****        ********  ********      ||                                          |\n"
        "||                                                                                           |      ****           **        **         ||                                          |\n"
        "||Donec tincidunt augue                                                                      |      ****           **        **         ||                                          |\n"
        "||sit amet metus                                                                             |      ****      **                        ||                                          |\n"
        "||pretium volutpat.                                                                          |       **********                         ||                                          |\n"
        "||Donec faucibus,                                                                            |        ********                          ||                                          |\n"
        "||ante sit amet                                                                              |                                          ||                                          |\n"
        "||luctus posuere,                                                                            |                                          ||                                          |\n"
        "||mauris tellus                                                                              |                                          ||                                          |\n"
        "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
        "||                                                                                       Bye,|*****   *      *  *      ******* ******  *||                                          |\n"
        "||                                                                                Hello Kitty|*    *  *      *  *      *            *  *||                                          |\n"
        "||                                                                                           |*    *  *      *  *      *           *   *||                                          |\n"
        "||                                                                                           |*    *  *      *  *      *****      *    *||                                          |\n"
        "||                                                                                           |****    *      *  *      *         *     *||                                          |\n"
        "||                                                                                           |*  *    *      *  *      *        *       ||                                          |\n"
        "||                                                                                           |*   *   *      *  *      *       *       *||                                          |\n"
        "||                                                                                           |*    *    *****   ****** ******* ******  *||                                          |\n"
        "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
        "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n"
        "|Lorem ipsum dolor sit amet,                                                                                                             |                                          |\n"
        "|consectetur adipiscing                                                                                                                  |                                          |\n"
        "|elit. Curabitur scelerisque                                                                                                             |        ********                          |\n"
        "|lorem vitae lectus cursus,                                                                                                              |       **********                         |\n"
        "|vitae porta ante placerat. Class aptent taciti                                                                                          |      **        **                        |\n"
        "|sociosqu ad litora                                                                                                                      |      **             **        **         |\n"
        "|torquent per                                                                                                                            |      **             **        **         |\n"
        "|conubia nostra,                                                                                                                         |      ***         ********  ********      |\n"
        "|per inceptos himenaeos.                                                                                                                 |      ****        ********  ********      |\n"
        "|                                                                                                                                        |      ****           **        **         |\n"
        "|Donec tincidunt augue                                                                                                                   |      ****           **        **         |\n"
        "|sit amet metus                                                                                                                          |      ****      **                        |\n"
        "|pretium volutpat.                                                                                                                       |       **********                         |\n"
        "|Donec faucibus,                                                                                                                         |        ********                          |\n"
        "|ante sit amet                                                                                                                           |                                          |\n"
        "|luctus posuere,                                                                                                                         |                                          |\n"
        "|mauris tellus                                                                                                                           |                                          |\n"
        "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n"
        "|                                                                                                                                    Bye,|*****   *      *  *      ******* ******  *|\n"
        "|                                                                                                                             Hello Kitty|*    *  *      *  *      *            *  *|\n"
        "|                                                                                                                                        |*    *  *      *  *      *           *   *|\n"
        "|                                                                                                                                        |*    *  *      *  *      *****      *    *|\n"
        "|                                                                                                                                        |****    *      *  *      *         *     *|\n"
        "|                                                                                                                                        |*  *    *      *  *      *        *       |\n"
        "|                                                                                                                                        |*   *   *      *  *      *       *       *|\n"
        "|                                                                                                                                        |*    *    *****   ****** ******* ******  *|\n"
        "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n" );

  return 0;
}
#endif /* __PROGTEST__ */
