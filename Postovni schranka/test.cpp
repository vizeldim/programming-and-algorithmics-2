#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
#endif /* __PROGTEST */
using namespace std;

#ifndef __PROGTEST__

//=========================CTimeStamp
  class CTimeStamp
  {
    public:
                    CTimeStamp                              ( int               year,
                                                              int               month,
                                                              int               day,
                                                              int               hour,
                                                              int               minute,
                                                              int               sec );
      int            Compare                                 ( const CTimeStamp & x ) const;
      friend ostream & operator <<                           ( ostream          & os,
                                                              const CTimeStamp & x )
      {
        return os << setfill('0') << x.m_Year <<"-" << setw(2) <<x.m_Month <<"-" << setw(2) <<x.m_Day << " " <<setw(2) << x.m_Hour << ":" <<setw(2) <<x.m_Minute << ":" <<setw(2) <<x.m_Sec;
      }
    private:
      int m_Year;
      int m_Month;
      int m_Day;
      int m_Hour;
      int m_Minute;
      int m_Sec;
  };

  CTimeStamp::CTimeStamp( int               year,
                          int               month,
                          int               day,
                          int               hour,
                          int               minute,
                          int               sec ): m_Year(year),
                                                    m_Month(month),
                                                    m_Day(day),
                                                    m_Hour(hour),
                                                    m_Minute(minute),
                                                    m_Sec(sec)
  {
  }

  int CTimeStamp::Compare( const CTimeStamp & x ) const
  {
    if(m_Year < x.m_Year){
      return -1;
    }
    else if(m_Year > x.m_Year)
    {
      return 1;
    }
    else if(m_Month < x.m_Month){
      return -1;
    }
    else if(m_Month > x.m_Month){
      return 1;
    }
    else if(m_Day < x.m_Day){
      return -1;
    }
    else if(m_Day > x.m_Day){
      return 1;
    }
    else if(m_Hour < x.m_Hour){
      return -1;
    }
    else if(m_Hour > x.m_Hour){
      return 1;
    }
    else if(m_Minute < x.m_Minute){
      return -1;
    }
    else if(m_Minute > x.m_Minute){
      return 1;
    }
    else if(m_Sec < x.m_Sec){
      return -1;
    }
    else if(m_Sec > x.m_Sec){
      return 1;
    }
    else{
      return 0;
    }
  }
//=========================CMailBody
class CMailBody
{
  public:
                   CMailBody                               ( int               size,
                                                             const char      * data );
     // copy cons/op=/destructor is correctly implemented in the testing environment
    friend ostream & operator <<                           ( ostream         & os,
                                                             const CMailBody & x )
    {
      return os << "mail body: " << x . m_Size << " B";
    }
  private:
    int            m_Size;
    char         * m_Data;
};

CMailBody::CMailBody( int               size,
                      const char      * data )
{
  m_Size = size;
}

//=================================================================================================
class CAttach
{
  public:
                   CAttach                                 ( int               x )
      : m_X (x),
        m_RefCnt ( 1 )
    {
    }
    void           AddRef                                  ( void ) const 
    { 
      m_RefCnt ++; 
    }
    void           Release                                 ( void ) const 
    { 
      if ( !--m_RefCnt ) 
        delete this; 
    }
  private:
    int            m_X;
    mutable int    m_RefCnt;
                   CAttach                                 ( const CAttach   & x );
    CAttach      & operator =                              ( const CAttach   & x );
                  ~CAttach                                 ( void ) = default;
    friend ostream & operator <<                           ( ostream         & os,
                                                             const CAttach   & x )
    {
      return os << "attachment: " << x . m_X << " B";
    }
};

CAttach::CAttach                                 ( const CAttach   & x )
{
  x.AddRef();
} 
//=================================================================================================
#endif /* __PROGTEST__, DO NOT remove */

class CMail
{
  public:
                   CMail                                   ( const CTimeStamp & timeStamp,
                                                             const string     & from,
                                                             const CMailBody  & body,
                                                             const CAttach    * attach );
    const string     & From                                ( void ) const;
    const CMailBody  & Body                                ( void ) const;
    const CTimeStamp & TimeStamp                           ( void ) const;
    const CAttach* Attachment                              ( void ) const;
    friend ostream & operator <<                           ( ostream          & os,
                                                             const CMail      & x );
    bool operator < ( const CMail      & other) const{
      return m_TimeStamp.Compare(other.m_TimeStamp) < 0;
    }
  private:
    CTimeStamp m_TimeStamp;
    string m_From;
    CMailBody m_Body;
   const CAttach * m_Attach;
};

CMail::CMail( const CTimeStamp & timeStamp,
              const string     & from,
              const CMailBody  & body,
              const CAttach    * attach ): m_TimeStamp(timeStamp),
                                           m_From(from),
                                           m_Body(body),
                                          m_Attach(attach)
                                          
{
  if(m_Attach!=nullptr){
    m_Attach -> AddRef();
  }
  
}

const string     & CMail::From                                ( void ) const
{
  return m_From;
}
const CMailBody  & CMail::Body                                ( void ) const
{
  return m_Body;
}
const CTimeStamp & CMail::TimeStamp                           ( void ) const
{
  return m_TimeStamp;
}
const CAttach * CMail::Attachment                              ( void ) const
{
  return m_Attach;
}

ostream & operator <<                           ( ostream          & os,
                                                  const CMail      & x )
{
  os << x.m_TimeStamp << " " << x.m_From << " " << x.m_Body;
  if(x.m_Attach != nullptr){
    os << " + " <<*(x.m_Attach);
  }
  return os;
}
//=================================================================================================


class CMailBox
{
  public:
                   CMailBox                                ( void );
    bool           Delivery                                ( const CMail      & mail );
    bool           NewFolder                               ( const string     & folderName );
    bool           MoveMail                                ( const string     & fromFolder,
                                                             const string     & toFolder );
    list<CMail>    ListMail                                ( const string     & folderName,
                                                             const CTimeStamp & from,
                                                             const CTimeStamp & to ) const;
    set<string>    ListAddr                                ( const CTimeStamp & from,
                                                             const CTimeStamp & to ) const;
  private:
    map<string,multiset<CMail>> Folders;
};

CMailBox::CMailBox( void )
{
  NewFolder("inbox");
}

bool CMailBox::NewFolder( const string     & folderName )
{
  map<string,multiset<CMail>>::iterator it; 
  it = Folders.find(folderName);
  if(it != Folders.end()){
    return false;
  }
  multiset<CMail> newMultiSet;
  Folders.insert(it, {folderName, newMultiSet});
  return true;
}

bool CMailBox::Delivery( const CMail      & mail )
{
  Folders["inbox"].insert(mail);
  return true;
}

bool CMailBox::MoveMail( const string     & fromFolder,
                         const string     & toFolder )
{
 if(Folders.find(fromFolder)==Folders.end() || Folders.find(toFolder)==Folders.end())
  {
    return false;
  }
  multiset<CMail> merged;
	merge(Folders[fromFolder].begin(), Folders[fromFolder].end(),
				Folders[toFolder].begin(), Folders[toFolder].end(),
				inserter(merged, merged.begin()));
  Folders[fromFolder].clear();
  Folders[toFolder] = merged;
  return true;
}

list<CMail>    CMailBox::ListMail                                ( const string     & folderName,
                                                             const CTimeStamp & from,
                                                             const CTimeStamp & to ) const
{

  

  list<CMail> empty;

  auto itFold = Folders.find(folderName);
  
  if(Folders.find(folderName)==Folders.end())
  {
    return empty;
  }
 
  multiset<CMail>::iterator itLow = itFold->second.lower_bound(CMail(from," ",CMailBody(1, "a"),nullptr));
  multiset<CMail>::iterator itUp =  itFold->second.upper_bound(CMail(to," ",CMailBody(1, "a"),nullptr));
  
  list<CMail> Mails(itLow, itUp);
  
  return Mails;
}

set<string>    CMailBox::ListAddr                                ( const CTimeStamp & from,
                                                          const CTimeStamp & to ) const
{
  multiset<CMail>::iterator itLow;
  multiset<CMail>::iterator itUp;  

  set<string> Users;
  for(auto Folder : Folders)
  {
    itLow = Folder.second.lower_bound(CMail(from," ",CMailBody(1, "a"),nullptr));
    itUp = Folder.second.upper_bound(CMail(to," ",CMailBody(1, "a"),nullptr));
    while(itLow != itUp){
      string a = itLow->From();
      Users.insert(a);
      itLow++;
    }
  }
  return Users;
}


//=================================================================================================
#ifndef __PROGTEST__
static string showMail ( const list<CMail> & l )
{
  ostringstream oss;
  for ( const auto & x : l )
    oss << x << endl;
  return oss . str ();
}
static string showUsers ( const set<string> & s )
{
  ostringstream oss;
  for ( const auto & x : s )
    oss << x << endl;
  return oss . str ();
}
int main ( void )
{
  list<CMail> mailList;
  set<string> users;
  CAttach   * att;

  CMailBox m0;
assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 24, 13 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 1" ), nullptr ) ) );
   assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 26, 23 ), "user2@fit.cvut.cz", CMailBody ( 22, "some different content" ), nullptr ) ) );
  att = new CAttach ( 200 );
  assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 11, 23, 43 ), "boss1@fit.cvut.cz", CMailBody ( 14, "urgent message" ), att ) ) );
  assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 18, 52, 27 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 2" ), att ) ) );
  att -> Release ();
  att = new CAttach ( 97 );
  assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 16, 12, 48 ), "boss1@fit.cvut.cz", CMailBody ( 24, "even more urgent message" ), att ) ) );
  att -> Release ();

assert ( showMail ( m0 . ListMail ( "inbox",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B + attachment: 200 B\n"
                        "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B\n"
                        "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B + attachment: 97 B\n"
                        "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 200 B\n" );
  assert ( showMail ( m0 . ListMail ( "inbox",
                      CTimeStamp ( 2014, 3, 31, 15, 26, 23 ),
                      CTimeStamp ( 2014, 3, 31, 16, 12, 48 ) ) ) == "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B\n"
                        "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B + attachment: 97 B\n" );
    assert ( showUsers ( m0 . ListAddr ( CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                       CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "boss1@fit.cvut.cz\n"
                        "user1@fit.cvut.cz\n"
                        "user2@fit.cvut.cz\n" );
 assert ( showUsers ( m0 . ListAddr ( CTimeStamp ( 2014, 3, 31, 15, 26, 23 ),
                       CTimeStamp ( 2014, 3, 31, 16, 12, 48 ) ) ) == "boss1@fit.cvut.cz\n"
                        "user2@fit.cvut.cz\n" );

  CMailBox m1;
  assert ( m1 . NewFolder ( "work" ) );
  assert ( m1 . NewFolder ( "spam" ) );
  assert ( !m1 . NewFolder ( "spam" ) );
 assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 24, 13 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 1" ), nullptr ) ) );
    att = new CAttach ( 500 );
  assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 26, 23 ), "user2@fit.cvut.cz", CMailBody ( 22, "some different content" ), att ) ) );
  att -> Release ();
  assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 11, 23, 43 ), "boss1@fit.cvut.cz", CMailBody ( 14, "urgent message" ), nullptr ) ) );
  att = new CAttach ( 468 );
  assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 18, 52, 27 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 2" ), att ) ) );
  att -> Release ();
  assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 16, 12, 48 ), "boss1@fit.cvut.cz", CMailBody ( 24, "even more urgent message" ), nullptr ) ) );
  assert ( showMail ( m1 . ListMail ( "inbox",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                        "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                        "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n" );
  assert ( showMail ( m1 . ListMail ( "work",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "" );
  assert ( m1 . MoveMail ( "inbox", "work" ) );
  cout << showMail ( m1 . ListMail ( "inbox",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) );
  assert ( showMail ( m1 . ListMail ( "inbox",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "" );
  assert ( showMail ( m1 . ListMail ( "work",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                        "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                        "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n" );
 assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 19, 24, 13 ), "user2@fit.cvut.cz", CMailBody ( 14, "mail content 4" ), nullptr ) ) );
  att = new CAttach ( 234 );
  assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 13, 26, 23 ), "user3@fit.cvut.cz", CMailBody ( 9, "complains" ), att ) ) );
  att -> Release ();
  assert ( showMail ( m1 . ListMail ( "inbox",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 13:26:23 user3@fit.cvut.cz mail body: 9 B + attachment: 234 B\n"
                        "2014-03-31 19:24:13 user2@fit.cvut.cz mail body: 14 B\n" );
  assert ( showMail ( m1 . ListMail ( "work",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                        "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                        "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n" );
  assert ( m1 . MoveMail ( "inbox", "work" ) );
 assert ( showMail ( m1 . ListMail ( "inbox",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "" );
  assert ( showMail ( m1 . ListMail ( "work",
                      CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                      CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 13:26:23 user3@fit.cvut.cz mail body: 9 B + attachment: 234 B\n"
                        "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                        "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                        "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                        "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n"
                        "2014-03-31 19:24:13 user2@fit.cvut.cz mail body: 14 B\n" );

  return 0;
}
#endif /* __PROGTEST__ */
