#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;
#endif /* __PROGTEST__ */

//---------------TRANSACTION
  struct CTransaction{
    CTransaction(const char * debAccID,
                const char * credAccID,
                unsigned int amount,
                const char * signature);
    ~CTransaction();
    bool deletable;
    char * debAccID;
    char * credAccID;
    unsigned int amount;
    char * signature;
  };
  CTransaction::CTransaction( const char * debAccID,
                              const char * credAccID,
                              unsigned int amount,
                              const char * signature): deletable(false)
  {
    this -> debAccID = new char[strlen(debAccID) + 1];
    this -> credAccID = new char[strlen(credAccID) + 1];
    this -> signature = new char[strlen(signature) + 1];
    strcpy(this -> debAccID, debAccID);
    strcpy(this -> credAccID, credAccID);
    strcpy(this -> signature, signature);
    this -> amount = amount;
  }

  CTransaction::~CTransaction(){
    if(deletable){
      delete [] debAccID;
      delete [] credAccID;
      delete [] signature;
    }
    else{
      deletable = true;
    }
  }

  

//---------------TRANSACTION_GROUP
 struct CTransactionGroup{
  CTransactionGroup();
  ~CTransactionGroup();
  void AddToGroup(CTransaction * newTransactionPointer);
  int TPsize;
  int sharedBy;
  CTransaction ** transactionPointers;
 }; 

  CTransactionGroup::CTransactionGroup(): TPsize(0),
                                          sharedBy(1),
                                          transactionPointers(nullptr)
  {}

  CTransactionGroup::~CTransactionGroup()
  {
    for(int i = 0; i < TPsize; i++){
      if(transactionPointers[i]->deletable)
      {
        delete transactionPointers[i];
      }
      else{
        transactionPointers[i]->deletable = true;
      }
    }
    delete [] transactionPointers;
  }

  void CTransactionGroup::AddToGroup(CTransaction * newTransactionPointer)
  {
    CTransaction ** nTP = new CTransaction * [TPsize+1];
    for(int i = 0; i < TPsize; i++){
        nTP[i] = transactionPointers[i];
    }
    delete [] transactionPointers;
    transactionPointers = nTP;
    TPsize++;
    transactionPointers[TPsize - 1] = newTransactionPointer;
  }

//---------------ACCOUNT
    struct CAccount{
      char * accID;
      int balance;
      int fBalance;
      int TGPsize;

      CTransactionGroup ** transactionGroupPointers;
      CAccount( const char * accID,
                int          initialBalance);
      CAccount():accID(nullptr),balance(0),fBalance(0), TGPsize(0), transactionGroupPointers(nullptr)
      {}               
      ~CAccount();
      inline int Balance(){return balance;}
      void operator = (CAccount & other){
          if(this != &other){ //SMAZAT!!!!!!!!!!!
            delete [] this -> accID;
            this -> accID = new char[strlen(other.accID) + 1];
            strcpy(this -> accID, other.accID);
          
            this -> fBalance = other.fBalance;
            this -> balance = other.balance;
            
            delete [] this -> transactionGroupPointers;
            this -> transactionGroupPointers = new CTransactionGroup *[other.TGPsize];
            for(int i = 0; i < other.TGPsize; i++)
            {
              this -> transactionGroupPointers[i] = other.transactionGroupPointers[i];
              other.transactionGroupPointers[i]->sharedBy++;
            }
            this -> TGPsize = other.TGPsize;
          }
      }
      friend ostream& operator<<(ostream& os, const CAccount& obj);
      void AddTransaction(CAccount * other,
                          unsigned int amount,
                          const char * signature);
    };

    ostream & operator<<(ostream& os, const CAccount& obj)
    {
      os << obj.accID <<":\n   ";
      os << obj.fBalance <<"\n";
      if(obj.transactionGroupPointers != nullptr)
      {
        for(int i = 0; i < obj.TGPsize; i++)
          {
            for(int j = 0; j < obj.transactionGroupPointers[i]->TPsize; j++)
            {
              if(strcmp(obj.transactionGroupPointers[i]->transactionPointers[j]->debAccID, obj.accID)==0){
                os << " - " << obj.transactionGroupPointers[i]->transactionPointers[j]->amount;
                os << ", to: " <<obj.transactionGroupPointers[i]->transactionPointers[j]->credAccID;
              }
              else{
                os << " + " << obj.transactionGroupPointers[i]->transactionPointers[j]->amount;
                os << ", from: " << obj.transactionGroupPointers[i]->transactionPointers[j]->debAccID;
              }
              os << ", sign: " << obj.transactionGroupPointers[i]->transactionPointers[j]->signature << "\n";
            }
          }
          
      }
      os<< " = "<<obj.balance<<"\n";
      return os;
    }

    CAccount::CAccount( const char * accID,
                        int          initialBalance):TGPsize(0), transactionGroupPointers(nullptr)
    {
        this -> accID = new char[strlen(accID)+1];
        strcpy(this->accID, accID);
        this -> balance = initialBalance;
        this -> fBalance = initialBalance;
    }


    CAccount::~CAccount(){
        delete [] accID;
        for(int i = 0; i < TGPsize; i++){
          if(transactionGroupPointers[i]->sharedBy == 1) 
          {
            delete transactionGroupPointers[i];
          }
          else{
            transactionGroupPointers[i]->sharedBy--;
          }
        }
        delete [] transactionGroupPointers;
    }

    void CAccount::AddTransaction(CAccount * other,
                                  unsigned int amount,
                                  const char * signature)
    {
      if(TGPsize == 0 || transactionGroupPointers[TGPsize - 1]->sharedBy > 1){
        CTransactionGroup ** nTGP = new CTransactionGroup * [TGPsize+1];
        for(int i = 0; i < TGPsize; i++){
            nTGP[i] = transactionGroupPointers[i];
        }
        nTGP[TGPsize] = new CTransactionGroup;
        delete [] transactionGroupPointers;
        transactionGroupPointers = nTGP;
        TGPsize++;
      }

      if(other -> TGPsize == 0 || other -> transactionGroupPointers[other->TGPsize - 1]->sharedBy > 1){
        CTransactionGroup ** nOTGP = new CTransactionGroup * [other->TGPsize+1];
        for(int i = 0; i < other -> TGPsize; i++){
            nOTGP[i] = other -> transactionGroupPointers[i];
        }
        nOTGP[other->TGPsize] = new CTransactionGroup;

        delete [] other -> transactionGroupPointers;
        other -> transactionGroupPointers = nOTGP;
        other -> TGPsize++;
      }
  
      CTransaction * newTransaction = new CTransaction(this->accID,other->accID, amount, signature);

      this -> balance -= amount;
      other -> balance += amount;

      this -> transactionGroupPointers[TGPsize-1]->AddToGroup(newTransaction); ///DODELAT!!!!
      (other -> transactionGroupPointers[other -> TGPsize-1])->AddToGroup(newTransaction);
    }
//---------------ACCOUNT_GROUP
    struct CAccountGroup{
      CAccountGroup();
      ~CAccountGroup();
      CAccount * AddAccount( const char * accID , int initialBalance );
      int FindInsertPos( const char * accID , int f, int l);

      
      int sharedBy;
      int Asize;
      CAccount * accs;
    };

    CAccountGroup::CAccountGroup(): sharedBy(1),
                                    Asize(0),
                                    accs(nullptr)
    {    
    }

    CAccountGroup::~CAccountGroup(){
        delete [] accs;
    }

    int CAccountGroup::FindInsertPos( const char * accID , int f, int l){
      if(f>=l){
        if(strcmp(accID, accs[f].accID)>0){
            return f+1;
          }else{
            return f;
          }
        }
      else{
        int m = (f+l)/2;
        if(strcmp(accID, accs[m].accID)>0){
          return FindInsertPos( accID, m+1, l);
        }
        else{
          return FindInsertPos( accID, f, m-1);
        }
      }
    }

    CAccount * CAccountGroup::AddAccount( const char * accID , int initialBalance  )
    { 
      int iPos = 0;
      if(Asize!=0){
          iPos = FindInsertPos(accID, 0, Asize - 1);
      }
      CAccount * nAccs = new CAccount [Asize+1];
      CAccount newAcc(accID, initialBalance);
      int i;
      for(i = 0; i < iPos; i++){
          nAccs[i] = accs[i]; 
      }
      nAccs[iPos] = newAcc;
      for(i = iPos; i < Asize; i++){
          nAccs[i + 1] = accs[i];
      }
      delete [] accs;
      accs = nAccs;
      Asize++;
      return (nAccs+iPos);
    }


//---------------BANK
  class CBank
  {
    public:
      CBank();
      CBank(const CBank & other);
      ~CBank();
      CBank & operator = (const CBank & other);
      void AddNewAccountGroup();
      CAccount * NewAccountUnSafe( const char * accID,
                                          int          initialBalance );
      bool   NewAccount (const char * accID,
                         int          initialBalance);
      bool   Transaction (const char * debAccID,
                          const char * credAccID,
                          unsigned int amount,
                          const char * signature );
      bool   TrimAccount ( const char * accID );
      CAccount & Account ( const char * accID )
      {
        CAccount * found = FindAcc(accID, AGPsize - 1, 0);
        if(found==nullptr)
        {
          throw invalid_argument("Account not found!");
        }
        return *found;
      }
    private:
      int AGPsize;
      CAccountGroup ** accGroupPointers;
      CAccount * FindAcc(const char * accID, int start, int end);
  };

  CBank::CBank(): AGPsize(0),
                  accGroupPointers(nullptr)
  {
  }

  CBank::CBank(const CBank & other)
  {
    AGPsize = other.AGPsize;
    if(AGPsize!=0)
    {
      accGroupPointers = new CAccountGroup * [AGPsize];
      for(int i = 0; i < AGPsize; i++)
      {
        accGroupPointers[i] = other.accGroupPointers[i];
        accGroupPointers[i] -> sharedBy++;
      }
    }
    else{
      accGroupPointers = nullptr;
    }
  }
  
  CBank::~CBank(){
    for(int i = 0; i < AGPsize; i++){
        if(accGroupPointers[i]->sharedBy == 1){
            delete accGroupPointers[i];
        }
        else{
          accGroupPointers[i]->sharedBy--;
        }
    }
    delete [] accGroupPointers;
  }

  CBank & CBank::operator = (const CBank & other)
  {
    if(this != &other){
      for(int i = 0; i < AGPsize; i++){
          if(accGroupPointers[i]->sharedBy == 1){
              delete accGroupPointers[i];
          }
          else{
            accGroupPointers[i]->sharedBy--;
          }
      }

      delete [] accGroupPointers;
      AGPsize = other.AGPsize;
      if(AGPsize!=0)
      {
        accGroupPointers = new CAccountGroup * [AGPsize];
        for(int i = 0; i < AGPsize; i++)
        {
          accGroupPointers[i] = other.accGroupPointers[i];
          accGroupPointers[i] -> sharedBy++;
        }
      }
      else{
        accGroupPointers = nullptr;
      }
    }
    return *this;
  }

  int cmpFunc(const void * x, const void * y)
  {
      const char * a = ((CAccount *) x) -> accID;
      const char * b = ((CAccount *) y) -> accID;
      return strcmp(a, b);
  }
    
  CAccount * CBank::FindAcc(const char * accID, int start, int end){
    CAccount searchItem;
    searchItem.accID = new char[strlen(accID)+1];
    strcpy(searchItem.accID, accID); 
    for(int i = start; i >= end; --i){
      CAccount * found = (CAccount *)bsearch (&searchItem, accGroupPointers[i]->accs, accGroupPointers[i]->Asize, sizeof(searchItem), cmpFunc);
      if(found!=nullptr){return found;}
    }
    return nullptr;
  }


  void CBank::AddNewAccountGroup()
  {
    CAccountGroup ** nAGP = new CAccountGroup * [AGPsize+1];
    for(int i = 0; i < AGPsize; i++){
        nAGP[i] = accGroupPointers[i];
    }
    nAGP[AGPsize] = new CAccountGroup;
    delete [] accGroupPointers;
    accGroupPointers = nAGP;
    AGPsize++;
  }
 

  CAccount * CBank::NewAccountUnSafe( const char * accID,
                                      int          initialBalance )
  {
    if(AGPsize == 0 || accGroupPointers[AGPsize - 1]->sharedBy > 1)
    {
      AddNewAccountGroup();    
    }
    return accGroupPointers[AGPsize-1]->AddAccount(accID, initialBalance);
  }


  bool CBank::NewAccount( const char * accID,
                          int          initialBalance )
  {
      if(FindAcc(accID,  AGPsize - 1, 0)!=nullptr){
          return false;
      }

      NewAccountUnSafe(accID, initialBalance);
      
      return true;

  }

  bool   CBank::Transaction   ( const char * debAccID,
                                const char * credAccID,
                                unsigned int amount,
                                const char * signature )
  {
    if(strcmp(debAccID, credAccID)==0 || AGPsize == 0){
      return false;
    } 

    CAccount searchItemDeb;
    searchItemDeb.accID = new char[strlen(debAccID)+1];
    strcpy(searchItemDeb.accID, debAccID); 

    CAccount searchItemCred;
    searchItemCred.accID = new char[strlen(credAccID)+1];
    strcpy(searchItemCred.accID, credAccID); 
    
    CAccount * foundDeb = nullptr;
    CAccount * foundCred = nullptr;

    int i = AGPsize - 1; // DEBPOS
    for(; i>=0; i--){
      foundDeb = (CAccount *)bsearch (&searchItemDeb, accGroupPointers[i]->accs, accGroupPointers[i]->Asize, sizeof(searchItemDeb), cmpFunc);
      if(foundDeb != nullptr){
        break;
      }
    }
    if(foundDeb == nullptr){
      return false;
    }
    
    int j = AGPsize - 1; //CREDPOS
    for(; j>=0; j--){
      foundCred = (CAccount *)bsearch (&searchItemCred, accGroupPointers[j]->accs, accGroupPointers[j]->Asize, sizeof(searchItemCred), cmpFunc);
      if(foundCred != nullptr){
        break;
      }
    }
    if(foundCred == nullptr){
      return false;
    }

    if(accGroupPointers[i]->sharedBy != 1)
    {
      CAccount * newFoundDeb = NewAccountUnSafe(debAccID,0);
      *newFoundDeb = *foundDeb;
    }

    if(accGroupPointers[j]->sharedBy != 1)
    {
      CAccount * newFoundCred = NewAccountUnSafe(credAccID,0);
      *newFoundCred = *foundCred;
      foundCred = newFoundCred;
    }
    foundCred = (CAccount *)bsearch (&searchItemCred, accGroupPointers[AGPsize-1]->accs, accGroupPointers[AGPsize-1]->Asize, sizeof(searchItemCred), cmpFunc);
    foundDeb = (CAccount *)bsearch (&searchItemDeb, accGroupPointers[AGPsize-1]->accs, accGroupPointers[AGPsize-1]->Asize, sizeof(searchItemDeb), cmpFunc);
    foundDeb->AddTransaction(foundCred, amount, signature);
    return true;
  }

  bool   CBank::TrimAccount ( const char * accID )
  {
    if(AGPsize==0){return false;}
    CAccount searchItem;
    searchItem.accID = new char[strlen(accID)+1];
    strcpy(searchItem.accID, accID); 
    CAccount * found = nullptr;
    int i = AGPsize - 1;
    for(; i>=0; i--){
      found = (CAccount *)bsearch (&searchItem, accGroupPointers[i]->accs, accGroupPointers[i]->Asize, sizeof(searchItem), cmpFunc);
      if(found != nullptr){
        break;
      }
    }
    
    if(found == nullptr){
      return false;
    }
    

    if(accGroupPointers[i]->sharedBy == 1)
    {
      for(int j = 0; j < found ->TGPsize; j++){
        if(found->transactionGroupPointers[j]->sharedBy==1){
          delete found->transactionGroupPointers[j];
        }
        else
        {
          found->transactionGroupPointers[j]->sharedBy--;
        }
      }
      delete [] found->transactionGroupPointers;
      found->transactionGroupPointers = nullptr;
      found ->TGPsize=0;

      found -> fBalance = found -> balance;
    }
    else
    {
      NewAccountUnSafe(found->accID,found->balance);
    }
    return true;
  }
#ifndef __PROGTEST__
int main ( void )
{
  ostringstream os;
  char accCpy[100], debCpy[100], credCpy[100], signCpy[100];
  CBank x0;
  assert ( x0 . NewAccount ( "123456", 1000 ) );
  assert ( x0 . NewAccount ( "987654", -500 ) );
  assert ( x0 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
  assert ( x0 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
  assert ( x0 . NewAccount ( "111111", 5000 ) );
  assert ( x0 . Transaction ( "111111", "987654", 290, "Okh6e+8rAiuT5=" ) );
 
  assert ( x0 . Account ( "123456" ). Balance ( ) ==  -2190 );
  assert ( x0 . Account ( "987654" ). Balance ( ) ==  2980 );
  assert ( x0 . Account ( "111111" ). Balance ( ) ==  4710 );
  os . str ( "" );
  os << x0 . Account ( "123456" );
  assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
  os . str ( "" );
  
  os << x0 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 290, from: 111111, sign: Okh6e+8rAiuT5=\n = 2980\n" ) );
  os . str ( "" );
  os << x0 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 290, to: 987654, sign: Okh6e+8rAiuT5=\n = 4710\n" ) );
  assert ( x0 . TrimAccount ( "987654" ) );
 
  assert ( x0 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
  os . str ( "" );
  os << x0 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   2980\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 3103\n" ) );

  CBank x2;
  strncpy ( accCpy, "123456", sizeof ( accCpy ) );
  assert ( x2 . NewAccount ( accCpy, 1000 ));
  strncpy ( accCpy, "987654", sizeof ( accCpy ) );
  assert ( x2 . NewAccount ( accCpy, -500 ));
  strncpy ( debCpy, "123456", sizeof ( debCpy ) );
  strncpy ( credCpy, "987654", sizeof ( credCpy ) );
  strncpy ( signCpy, "XAbG5uKz6E=", sizeof ( signCpy ) );
  assert ( x2 . Transaction ( debCpy, credCpy, 300, signCpy ) );
  strncpy ( debCpy, "123456", sizeof ( debCpy ) );
  strncpy ( credCpy, "987654", sizeof ( credCpy ) );
  strncpy ( signCpy, "AbG5uKz6E=", sizeof ( signCpy ) );
  assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
  strncpy ( accCpy, "111111", sizeof ( accCpy ) );
  assert ( x2 . NewAccount ( accCpy, 5000 ));
  strncpy ( debCpy, "111111", sizeof ( debCpy ) );
  strncpy ( credCpy, "987654", sizeof ( credCpy ) );
  strncpy ( signCpy, "Okh6e+8rAiuT5=", sizeof ( signCpy ) );
  assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
  assert ( x2 . Account ( "123456" ). Balance ( ) ==  -2190 );
  assert ( x2 . Account ( "987654" ). Balance ( ) ==  5580 );
  assert ( x2 . Account ( "111111" ). Balance ( ) ==  2110 );
  os . str ( "" );
  os << x2 . Account ( "123456" );
  assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
  os . str ( "" );
  os << x2 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n = 5580\n" ) );
  os . str ( "" );
  os << x2 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n = 2110\n" ) );
  assert ( x2 . TrimAccount ( "987654" ) );
  strncpy ( debCpy, "111111", sizeof ( debCpy ) );
  strncpy ( credCpy, "987654", sizeof ( credCpy ) );
  strncpy ( signCpy, "asdf78wrnASDT3W", sizeof ( signCpy ) );
  assert ( x2 . Transaction ( debCpy, credCpy, 123, signCpy ) );
  os . str ( "" );
  os << x2 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   5580\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );

  CBank x4;
  assert ( x4 . NewAccount ( "123456", 1000 ) );
  assert ( x4 . NewAccount ( "987654", -500 ) );
  assert ( !x4 . NewAccount ( "123456", 3000 ) );
  assert ( !x4 . Transaction ( "123456", "666", 100, "123nr6dfqkwbv5" ) );
  assert ( !x4 . Transaction ( "666", "123456", 100, "34dGD74JsdfKGH" ) );
  assert ( !x4 . Transaction ( "123456", "123456", 100, "Juaw7Jasdkjb5" ) );
  try
  {
    x4 . Account ( "666" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
  }
  catch ( ... )
  {
  }
  try
  {
    os << x4 . Account ( "666" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
  }
  catch ( ... )
  {
  }
  assert ( !x4 . TrimAccount ( "666" ) );

  CBank x6;
  assert ( x6 . NewAccount ( "123456", 1000 ) );
  assert ( x6 . NewAccount ( "987654", -500 ) );
  assert ( x6 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
  assert ( x6 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
  assert ( x6 . NewAccount ( "111111", 5000 ) );
  assert ( x6 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
  
  CBank x7 ( x6 );
  assert ( x6 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );

  assert ( x7 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
  assert ( x6 . NewAccount ( "99999999", 7000 ) );
  assert ( x6 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
  assert ( x6 . TrimAccount ( "111111" ) );
  assert ( x6 . Transaction ( "123456", "111111", 221, "Q23wr234ER==" ) );

  os . str ( "" );
  os << x6 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n + 221, from: 123456, sign: Q23wr234ER==\n = -1581\n" ) );
  os . str ( "" );
  os << x6 . Account ( "99999999" );
  assert ( ! strcmp ( os . str () . c_str (), "99999999:\n   7000\n + 3789, from: 111111, sign: aher5asdVsAD\n = 10789\n" ) );
  os . str ( "" );
  os << x6 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );
  os . str ( "" );
  os << x7 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );
  try
  {
    os << x7 . Account ( "99999999" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
  }
  catch ( ... )
  {
  }
  os . str ( "" );
  os << x7 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 789, from: 111111, sign: SGDFTYE3sdfsd3W\n = 6369\n" ) );

  CBank x8;
  CBank x9;
  assert ( x8 . NewAccount ( "123456", 1000 ) );
  assert ( x8 . NewAccount ( "987654", -500 ) );
  assert ( x8 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
  assert ( x8 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
  assert ( x8 . NewAccount ( "111111", 5000 ) );
  assert ( x8 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );

  x9 = x8;
  assert ( x8 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
  assert ( x9 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
  assert ( x8 . NewAccount ( "99999999", 7000 ) );
  assert ( x8 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
  assert ( x8 . TrimAccount ( "111111" ) );
  os . str ( "" );
  os << x8 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n = -1802\n" ) );
  os . str ( "" );
  os << x9 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );
  return 0;
}
#endif /* __PROGTEST__ */
