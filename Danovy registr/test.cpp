#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <stdexcept>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

/*----------------HUMAN -> NAME, ADDRESS, ACCOUNT ________________________*/
struct AHuman{
  public:
    AHuman(string name, string addr, string account);
    string getName()const{return name;}
    string getAddr()const{return addr;}
    string getAccount()const{return account;}
  private:
    string name;
    string addr;
    string account;
};

AHuman::AHuman(string name, string addr, string account){
  this -> name = name;
  this -> addr = addr; 
  this -> account = account;
}


/*----------------HUMAN ----------------------------------*/
class Human{
  public:
    Human(string name, string addr, string account);
    string getName()const{return name;}
    string getAddr()const{return addr;}
    string getAccount()const{return account;}
    int getIncome()const{return income;}
    int getExpense()const{return expense;}
    void addIncome(int amount){this -> income += amount;}
    void addExpense(int amount){this -> expense += amount;}
  private:
    string                   name;
    string                   addr;
    string                   account;
    int                      income;
    int                      expense;
};


Human::Human(string name, string addr, string account): income(0), expense(0){
  this -> name = name;
  this -> addr = addr; 
  this -> account = account;
}



/*---------------------CITERATOR-----------------------*/
class CIterator
{
  public:
    CIterator();
    bool                     AtEnd                         ( void ) const{return pos==DB.size();};
    void                     Next                          ( void ) {pos++;}
    string                   Name                          ( void ) const {return DB[pos].getName();}
    string                   Addr                          ( void ) const {return DB[pos].getAddr();}
    string                   Account                       ( void ) const {return DB[pos].getAccount();}
    void                     setDB                         ( const vector<Human> & DB){this -> DB = DB;}
  private:
    vector<Human> DB;
    unsigned int pos;
};

CIterator::CIterator():pos(0){}



/*-----------------------CTAXREGISTER----------------------------------------------------------*/
class CTaxRegister
{
  public:
    bool                     Birth                         ( const string    & name,
                                                             const string    & addr,
                                                             const string    & account );
    bool                     Death                         ( const string    & name,
                                                             const string    & addr );
    bool                     Income                        ( const string    & account,
                                                             int               amount );
    bool                     Income                        ( const string    & name,
                                                             const string    & addr,
                                                             int               amount );
    bool                     Expense                       ( const string    & account,
                                                             int               amount );
    bool                     Expense                       ( const string    & name,
                                                             const string    & addr,
                                                             int               amount );
    bool                     Audit                         ( const string    & name,
                                                             const string    & addr,
                                                             string          & account,
                                                             int             & sumIncome,
                                                             int             & sumExpense ) const;
    CIterator                ListByName                    ( void ) const;
    void print();
  private:
    // todo
    int findPos(string name, string addr, int f, int l) const;
    int findPos(string account, int f, int l) const;

    vector<Human> DB;
    vector<AHuman> ACC;
};

bool CTaxRegister::Birth (const string    & name,
                          const string    & addr,
                          const string    & account)
{
  if(DB.size()==0){
    DB.push_back(Human(name, addr, account));
    ACC.push_back(AHuman(name, addr, account));
    return true;
  }
  

  int posC = findPos(name, addr, 0, DB.size()-1);
  int posA = findPos(account,0,ACC.size()-1);

  if( posA < 0 || posC < 0){
    return false;
  }


   if(posC >= (int)DB.size()){
    DB.push_back(Human(name, addr, account));
 }else{
   DB.insert(DB.begin()+posC, Human(name, addr, account));

 }
  if(posA >= (int)ACC.size()){
      ACC.push_back(AHuman(name, addr, account));
  }
else{
ACC.insert(ACC.begin()+posA, AHuman(name, addr, account));
}
  //cout << posC <<endl;

  
   //DB.push_back(Human(name, addr, account));
   // ACC.push_back(AHuman(name, addr, account));
  return true;
}

CIterator CTaxRegister::ListByName( void ) const{
  CIterator it;
  it.setDB(DB); 
  return it;
}



int CTaxRegister::findPos( string name, string addr , int f, int l)const{
    int m = (l+f)/2; 
  if(f>=l){
    if(name == DB[m].getName() && addr == DB[m].getAddr()){
      return -(m+1); 
    }
    return (name > DB[f].getName()) || (name == DB[f].getName() && addr > DB[f].getAddr())? (f+1) : f;
  }
  if(name == DB[m].getName() && addr == DB[m].getAddr()){
    return -(m+1); 
  }
  if(name > DB[m].getName() ||  (name == DB[m].getName() && addr > DB[m].getAddr())){
    return findPos(name, addr, m+1, l); 
  }
  else{
    return findPos(name, addr, f, m-1); 
  }  

}


int CTaxRegister::findPos(string account, int f, int l)const{
   int m = (l+f)/2; 
  if(f>=l){
    if(account == ACC[m].getAccount()){
      return -(m+1); 
    }
    return (account > ACC[f].getAccount())? (f+1) : f;
  }

  if(account == ACC[m].getAccount()){
    return -(m+1); 
  }
  if(account > ACC[m].getAccount()){
    return findPos(account, m+1, l); 
  }
  else{
    return findPos(account, f, m-1); 
  }  

}

bool CTaxRegister::Death( const string    & name,
                          const string    & addr ){

  if(DB.size()==0){return false;}
  int pos = findPos(name, addr, 0, DB.size()-1);
  if(pos >= 0){return false;}
  pos = -pos -1;

  int npos = findPos(DB[pos].getAccount(), 0, DB.size()-1 );
   //cout << pos <<endl;
  npos = -npos-1;
  DB.erase(DB.begin()+pos);
   ACC.erase(ACC.begin()+npos);
  
  return true;
}

bool CTaxRegister::Income  ( const string    & name,
                             const string    & addr,
                            int               amount )
{
 
   if(amount < 0){return false;}
  int pos = findPos(name, addr, 0, DB.size()-1);
  
  if(pos >= 0){return false;}
  pos = -pos -1;
 
  DB[pos].addIncome(amount);
  return true;
}

bool CTaxRegister::Income ( const string    & account,
                            int               amount )
{
  
  if(amount < 0){return false;}
  int pos = findPos(account, 0, DB.size()-1);
   //cout << pos <<endl;
  if(pos >= 0){return false;}
  pos = -pos -1;
  //cout << ACC[pos].getName() << "  "<<ACC[pos].getAddr() <<endl;
  int npos = findPos(ACC[pos].getName(), ACC[pos].getAddr(),0, DB.size()-1 );
   //cout << pos <<endl;
  DB[-npos-1].addIncome(amount);
 
  return true;
}


bool CTaxRegister::Expense  (const string    & name,
                             const string    & addr,
                             int               amount )
{
    if(amount < 0){return false;}
  int pos = findPos(name, addr, 0, DB.size()-1);
  
  if(pos >= 0){return false;}
  pos = -pos -1;
 
  DB[pos].addExpense(amount);
  return true;
}

bool CTaxRegister::Expense (const string    & account,
                            int               amount )
{
   if(amount < 0){return false;}
  int pos = findPos(account, 0, DB.size()-1);
   //cout << pos <<endl;
  if(pos >= 0){return false;}
  pos = -pos -1;
  //cout << ACC[pos].getName() << "  "<<ACC[pos].getAddr() <<endl;
  int npos = findPos(ACC[pos].getName(), ACC[pos].getAddr(),0, DB.size()-1 );
   //cout << pos <<endl;
  DB[-npos-1].addExpense(amount);
 return true;
}


bool CTaxRegister::Audit (const string    & name,
                          const string    & addr,
                          string          & account,
                          int             & sumIncome,
                          int             & sumExpense ) const
{
   
    int pos = findPos(name, addr, 0, DB.size()-1);
   if(pos >= 0){return false;}
  pos = -pos -1;
  account = DB[pos].getAccount();
  sumIncome = DB[pos].getIncome();
  sumExpense = DB[pos].getExpense();
  return true;
}


#ifndef __PROGTEST__


void CTaxRegister::print(){
  CIterator tst = ListByName();
  for(unsigned int i = 0; i<DB.size(); i++){
    cout << tst.Name() << "\t\t";
    cout << tst.Addr() << "\t\t";
    cout << tst.Account() <<"\t\t";
    cout << DB[i].getIncome() <<endl;
    tst.Next();
  }
  cout << endl;
for(unsigned int i = 0; i<ACC.size(); i++){
    cout << ACC[i].getName() <<"\t\t";
    cout << ACC[i].getAddr() <<"\t\t";
    cout << ACC[i].getAccount() <<endl;
  }
   

}

int main ( void )
{
  string acct;
  int    sumIncome, sumExpense;
  CTaxRegister b1;

  assert ( b1 . Birth ( "John Smith", "Oak Road 23", "123/456/789" ) );
  assert ( b1 . Birth ( "Jane Hacker", "Main Street 17", "Xuj5#94" ) );
  assert ( b1 . Birth ( "Peter Hacker", "Main Street 17", "634oddT" ) );
  assert ( b1 . Birth ( "John Smith", "Main Street 17", "Z343rwZ" ) );




 assert ( b1 . Income ( "Xuj5#94", 1000 ) );


 assert ( b1 . Income ( "634oddT", 2000 ) );
  assert ( b1 . Income ( "123/456/789", 3000 ) );

 
  assert ( b1 . Income ( "634oddT", 4000 ) );
 
 assert ( b1 . Income ( "Peter Hacker", "Main Street 17", 2000 ) );

 

 
 assert ( b1 . Expense ( "Jane Hacker", "Main Street 17", 2000 ) );
 
  assert ( b1 . Expense ( "John Smith", "Main Street 17", 500 ) );
  
  assert ( b1 . Expense ( "Jane Hacker", "Main Street 17", 1000 ) );
 
  assert ( b1 . Expense ( "Xuj5#94", 1300 ) );
     


 
 assert ( b1 . Audit ( "John Smith", "Oak Road 23", acct, sumIncome, sumExpense ) );
  assert ( acct == "123/456/789" );
  assert ( sumIncome == 3000 );
  assert ( sumExpense == 0 );
  

   
  assert ( b1 . Audit ( "Jane Hacker", "Main Street 17", acct, sumIncome, sumExpense ) );
  assert ( acct == "Xuj5#94" );
  
  
  assert ( sumIncome == 1000 );
  assert ( sumExpense == 4300 );
  assert ( b1 . Audit ( "Peter Hacker", "Main Street 17", acct, sumIncome, sumExpense ) );
  assert ( acct == "634oddT" );
  assert ( sumIncome == 8000 );
  assert ( sumExpense == 0 );
  assert ( b1 . Audit ( "John Smith", "Main Street 17", acct, sumIncome, sumExpense ) );
  assert ( acct == "Z343rwZ" );
  assert ( sumIncome == 0 );
  assert ( sumExpense == 500 );
  CIterator it = b1 . ListByName ();
  assert ( ! it . AtEnd ()
           && it . Name () == "Jane Hacker"
           && it . Addr () == "Main Street 17"
           && it . Account () == "Xuj5#94" );
  it . Next ();
  assert ( ! it . AtEnd ()
           && it . Name () == "John Smith"
           && it . Addr () == "Main Street 17"
           && it . Account () == "Z343rwZ" );
  it . Next ();
  assert ( ! it . AtEnd ()
           && it . Name () == "John Smith"
           && it . Addr () == "Oak Road 23"
           && it . Account () == "123/456/789" );
  it . Next ();
  assert ( ! it . AtEnd ()
           && it . Name () == "Peter Hacker"
           && it . Addr () == "Main Street 17"
           && it . Account () == "634oddT" );
  it . Next ();
  assert ( it . AtEnd () );

  assert ( b1 . Death ( "John Smith", "Main Street 17" ) );

  CTaxRegister b2;
  assert ( b2 . Birth ( "John Smith", "Oak Road 23", "123/456/789" ) );
  assert ( b2 . Birth ( "Jane Hacker", "Main Street 17", "Xuj5#94" ) );
  assert ( !b2 . Income ( "634oddT", 4000 ) );
  assert ( !b2 . Expense ( "John Smith", "Main Street 18", 500 ) );
  assert ( !b2 . Audit ( "John Nowak", "Second Street 23", acct, sumIncome, sumExpense ) );
  assert ( !b2 . Death ( "Peter Nowak", "5-th Avenue" ) );
  assert ( !b2 . Birth ( "Jane Hacker", "Main Street 17", "4et689A" ) );
  assert ( !b2 . Birth ( "Joe Hacker", "Elm Street 23", "Xuj5#94" ) );
  assert ( b2 . Death ( "Jane Hacker", "Main Street 17" ) );
  
  assert ( b2 . Birth ( "Joe Hacker", "Elm Street 23", "Xuj5#94" ) );
  assert ( b2 . Audit ( "Joe Hacker", "Elm Street 23", acct, sumIncome, sumExpense ) );
  assert ( acct == "Xuj5#94" );
  assert ( sumIncome == 0 );
  assert ( sumExpense == 0 );
  assert ( !b2 . Birth ( "Joe Hacker", "Elm Street 23", "AAj5#94" ) );

  return 0;
}
#endif /* __PROGTEST__ */
