#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <map>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
using namespace std;
#endif /* __PROGTEST__ */

struct TNODE{
	int dataN;
	char data[4];
	TNODE * left;
	TNODE * right;
};

class Translator{
private:
	int pos;
	char oneByte;
	int numToTranslate;
	ifstream inFile;
	ofstream outFile;
	TNODE * m_root;
	bool moreChars;
	bool validDictionary;
	bool checkStream();
	int powTwo(int x);
	bool validateUTF8(TNODE * root);
	bool validateEnd();
	bool readChar(TNODE * root);
	bool createDictionary(TNODE * root);
	bool decode(TNODE * root);
	bool readChunk();
	void freeDictionary(TNODE * root);
public:
	Translator(const char * inFileName, const char * outFileName);
	~Translator();
	bool translate();
};


//Constructor -------------------
Translator::Translator(const char * inFileName, const char * outFileName ):pos(8),
																		   																		moreChars(true),
																																					validDictionary(true)
{
	inFile.open(inFileName, ios::binary);
	outFile.open(outFileName, ios::binary);
}

//Destructor --------------------
Translator::~Translator(){
	inFile.close();
	outFile.close();
}

void Translator::freeDictionary(TNODE * root){
	if(root->left != nullptr){
		freeDictionary(root->left);
	}
	if(root->right != nullptr){
		freeDictionary(root->right);
	}
	delete root;
}



bool Translator::checkStream(){
		if( inFile.fail() || outFile.fail() ){
			return false;
		}
		return true;
}

bool Translator::createDictionary(TNODE * root){
	if(pos==8)
	{
		if(!(inFile.get(oneByte)))
		{
			return false;
		}
		pos = 0;
	}
	root->right = nullptr;
	root->left = nullptr;
	if(((oneByte >> (7-pos) ) & 1))
	{
    pos++;
		if(!readChar(root) || !(validateUTF8(root)))
		{
			validDictionary = false;
			return false;
		}


  }else{
			pos++;
      root -> left = new TNODE;
      root -> right = new TNODE;

      createDictionary(root -> left);
      createDictionary(root -> right);
  }
	return validDictionary;
}



int Translator::powTwo(int x){
	int y = 1;
	for(int i = 0; i<x; i++){
		y*=2;
	}
	return y;
}



bool Translator::validateUTF8(TNODE * root){
	if(root->data[0]<0){
 		if(root->dataN == 1){return false;} 

		 if(root->dataN == 2){
			if(!((root->data[1] >> (7) ) & 1) || ((root->data[1] >> (6) ) & 1)){return false;}
		}
		if(root->dataN == 3){
			if(!((root->data[1] >> (7) ) & 1) || ((root->data[1] >> (6) ) & 1)){return false;}
			if(!((root->data[2] >> (7) ) & 1) || ((root->data[2] >> (6) ) & 1)){return false;}
		}
		if(root->dataN == 4){
			if(!((root->data[1] >> (7) ) & 1) || ((root->data[1] >> (6) ) & 1)){return false;}
			if(!((root->data[2] >> (7) ) & 1) || ((root->data[2] >> (6) ) & 1)){return false;}
			if(!((root->data[3] >> (7) ) & 1) || ((root->data[3] >> (6) ) & 1)){return false;}
		}
	}

	if(root->dataN==4){
		unsigned int val=0;
		unsigned int val0=256+root->data[0];;
		unsigned int val1=256+root->data[1];
		unsigned int val2=256+root->data[2];
		unsigned int val3=256+root->data[3];

		val0 <<= 24;
		val1 <<= 16;
		val2 <<= 8;

		val |= val0 | val1 |val2 |val3;
		unsigned int maxVal = 4103061439;

		if(val > maxVal){
			return false;
		}
	}
	return true;
}


bool Translator::validateEnd(){

	if(pos != 8){
		for(int i = 7-pos; i>=0; i--){
			if(((oneByte >> (7-pos) ) & 1)){
				return false;
			}
		}
		if(inFile.get(oneByte)){
			return false;
		}
	}

	else if(inFile.get(oneByte)){
		return false;
	}
	return true;
}



bool Translator::readChar(TNODE *root){
	int cnt = 0;
	for(int i = (7-pos); i>=0; i--)
	{
		cnt++;
		pos++;
		root -> data[0] = (root->data[0] << 1) | ((oneByte >> i ) & 1);
	}
	if(cnt!=8)
	{
		if(!inFile.get(oneByte)){
			return false;
		}
		int tmpcnt = 8-cnt;
		for(int i = 7; i>(7-tmpcnt); i--)
		{
			cnt++;
			root -> data[0] = (root -> data[0] << 1) | ((oneByte >> i ) & 1);
		}
		pos = tmpcnt;
	}

	root -> dataN = 1;

	if((root -> data[0])<0){

		while(((root -> data[0] >> (7-(root -> dataN)) ) & 1)){
			++(root -> dataN);
		}

		if((root -> dataN) > 4){
			return false;
		}

		for(int j = 1; j < (root -> dataN); j++){
			int cnt = 0;
			for(int i = (7-pos); i>=0; i--)
			{
				cnt++;
				pos++;
				root->data[j] = (root->data[j] << 1) | ((oneByte >> i ) & 1);

			}

			if(cnt!=8)
			{
				pos=0;
				if(!inFile.get(oneByte)){
					return false;
				}

				int tmpcnt = 8-cnt;
				for(int i = 7; i>(7-tmpcnt); i--)
				{
					cnt++;
					root->data[j] = (root->data[j] << 1) | ((oneByte >> i ) & 1);
				}
				pos = tmpcnt;
			}
		}
	}
	return true;
}


bool Translator::decode(TNODE *root){
	if(numToTranslate>0){
	  if(root->right == nullptr && root -> left == nullptr){
			for(int i = 0; i<root->dataN; i++){
				outFile << root -> data[i];
				if(outFile.fail()){
					return false;
				}
			}
			numToTranslate--;
	    return decode(m_root);
	  }
		else{
			if(pos==8){
				if(!inFile.get(oneByte)){
					return false;
				}
				pos = 0;
			}
			if(((oneByte >> (7-pos) ) & 1)){
				pos++;
				if(root->right == nullptr){return false;}
				return decode(root -> right);
			}
			else{
				pos++;
				if(root->left==nullptr){return false;}
				return decode(root -> left);
			}
		}
	}
	return true;
}


////////////////////////(num<=0)||------------------------CHUNKYYY

bool Translator::translate(){
	 if(!(checkStream())){
		 return false;
	 }
	 m_root = new TNODE;
	 m_root -> left = nullptr;
	 m_root -> right = nullptr;

	 if( !createDictionary(m_root)){
		 return false;
	 }


   while(moreChars){
 		if(pos==8){
			if(!inFile.get(oneByte)){
				return false;
			}
 			pos=0;
 		}
     if( !(readChunk()) ||  !(decode(m_root))){
			 freeDictionary(m_root);
 			return false;
 		}
   }

 	if(!validateEnd()){
		freeDictionary(m_root);
 		return false;
 	}
	freeDictionary(m_root);

	 return true;
}


bool Translator::readChunk(){
  if(!((oneByte >> (7-pos) ) & 1)){
		numToTranslate = 0;
		pos++; // precti bud 0 nebo 1(podminka)

    int cnt = 0;
    for(int i = 7-pos; i>=0; i--){
      pos++;
      cnt++;
      if(((oneByte >> i ) & 1)){
        numToTranslate += powTwo((12-cnt));
      }
    }
    while(cnt!=12){
			if(!inFile.get(oneByte)){
				return false;
			}
      pos=0;
      if(cnt <= 4){
        for(int i = 7; i>=0; i--){
          pos++;
          cnt++;
          if(((oneByte >> i ) & 1)){
            numToTranslate += powTwo((12-cnt));
          }
        }

      }else{
        int tmp = 8-(12-cnt);
        for(int i = 7; i>=tmp; i--){
          pos++;
          cnt++;
          if(((oneByte >> i ) & 1)){
            numToTranslate += powTwo((12-cnt));
          }
          if(cnt==12){cnt=12;break;}
        }
      }
    }
	moreChars = false;
  }else{
	pos++; // precti bud 0 nebo 1(podminka)
    numToTranslate = 4096;
  }
	return true;
}







bool decompressFile ( const char * inFileName, const char * outFileName )
{
  // todo
	Translator t(inFileName, outFileName);
  return t.translate();
}

bool compressFile ( const char * inFileName, const char * outFileName )
{
  return false;
}
#ifndef __PROGTEST__
bool identicalFiles ( const char * fileName1, const char * fileName2 )
{
  // todo
  ifstream inFile1(fileName1);
  ifstream inFile2(fileName2);

  char a, b;
  while(1){
        inFile1.get(a);
        inFile2.get(b);
        if (inFile1.eof() && inFile2.eof() && a == b) {
            return true;
        } else if (inFile1.eof() || inFile2.eof()) {
            return false;
        }
    }
    return false;
}


int main ( void )
{


  assert ( decompressFile ( "tests/test0.huf", "tempfile" ) );
 assert ( identicalFiles ( "tests/test0.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test1.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test1.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test2.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test2.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test3.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test3.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/test4.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/test4.orig", "tempfile" ) );

 assert ( ! decompressFile ( "tests/test5.huf", "tempfile" ) );


  assert ( decompressFile ( "tests/extra0.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra0.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra1.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra1.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra2.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra2.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra3.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra3.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra4.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra4.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra5.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra5.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra6.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra6.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra7.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra7.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra8.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra8.orig", "tempfile" ) );

  assert ( decompressFile ( "tests/extra9.huf", "tempfile" ) );
  assert ( identicalFiles ( "tests/extra9.orig", "tempfile" ) );
/**/
  return 0;
}
#endif /* __PROGTEST__ */
